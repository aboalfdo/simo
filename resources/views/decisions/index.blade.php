@extends('layouts.players')

@section('content')
    <section class="content">
        @if(Session::has('alert-success'))
            <div class="alert alert-success"><i class="fa fa-check" aria-hidden="true"></i> <strong>{!! session('alert-success') !!}</strong></div>
        @endif
        @if(Session::has('alert-danger'))
            <div class="alert alert-danger"><i class="fa fa-times" aria-hidden="true"></i> <strong>{!! session('alert-danger') !!}</strong></div>
        @endif
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header col-lg-9 float-right">
                        <h3 class="box-title ">جدول القرارات للشركة : <b style="color:green"> {{$company_name}}</b></h3>
                    </div><!-- /.box-header -->
                    <div class="col-lg-3 float-left pt10">
                        <a class="btn btn-block btn-primary " href="{{url('decisions/create')}}">إضافة قرارات</a>
                    </div>
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>اسم المنتج </th>
                                <th>الكمية المصنعة في المنشأة من المنتج </th>
                                <th> السعر المعتمد في المنشأة لبيع المنتج</th>
                                <th>الإنفاق التسويقي للمنشأة على المنتج </th>
                                <th>الإنفاق الاستثماري للمنشأة على المنتج </th>

                                <th>الجولة </th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($decisions as $decision)
                            <tr>
                                <td>{{$decision->product->name}}</td>
                                <td>{{$decision->Q}}</td>
                                <td>{{$decision->P}}</td>
                                <td>{{$decision->Mkg}}</td>
                                <td>{{$decision->Inv}}</td>

                                <td>{{$decision->phase}}</td>

                                 <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-default">Action</button>
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                            <span class="caret"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="#">حذف</a></li>

                                        </ul>
                                    </div>
                                </td>

                            </tr>
                            @endforeach
                            </tbody>
                            <tfoot>

                            </tfoot>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->

            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
@endsection

@push('footer-script')
    <script>
        $(function () {

            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false
            });
        });
    </script>
@endpush
