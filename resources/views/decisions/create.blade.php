@extends('layouts.players')

@section('content')
<section class="content">
<div class="row">
<div class="col-md-12">
    <!-- general form elements -->
    <div class="box box-primary">
        <div class="box-header with-border">
            <h4 style="direction: rtl" class="">
                <div class="col-lg-4 text-center">الجولة : {{$phase+1}} </div>
                    <div class="col-lg-4 text-center">اللاعب : {{$company_name}}</div>
                <div class="col-lg-4 text-center"> إضافة القرارات</div>
            </h4>
        </div><!-- /.box-header -->
        <!-- form start -->
        <form role="form" action="{{url('decisions/store')}}" method="post">
            {{ csrf_field() }}
            <input type="hidden" name="game_id" value="{{$game_id}}">
            <input type="hidden" name="phase" value="{{$phase+1}}">
            @for($i=0;$i<$main_parameters->M;$i++)
            <div class="box-body">

                <div class="form-group">
                    <h4 style="font-style: italic"> المنتج {{$products[$i]->name}}</h4>
                    <input type="hidden" name="product_id[]" class="form-control" value="{{$products[$i]->id}}" >
                </div>
                <div class="form-group">
                    <label for="q">الكمية المصنعة في المنشأة من المنتج</label>
                    <input type="text" name="q[]" class="form-control" id="q" placeholder="">
                </div>
                <div class="form-group">
                    <label for="p">السعر المعتمد في المنشأة لبيع المنتج</label>
                    <input type="text" name="p[]" class="form-control" id="p" placeholder="">
                </div>
                <div class="form-group">
                    <label for="mkg">الإنفاق التسويقي للمنشأة على المنتج</label>
                    <input type="text" class="form-control" name="mkg[]" id="mkg" placeholder="">
                </div>
                <div class="form-group">
                    <label for="inv">الإنفاق الاستثماري للمنشأة على المنتج</label>
                    <input type="text" class="form-control" name="inv[]" id="inv" placeholder="">
                </div>

            </div><!-- /.box-body -->
            @endfor

            <div class="box-footer">
                <button type="submit" class="btn btn-primary">حفظ</button>
            </div>
        </form>
    </div><!-- /.box -->


</div><!--/.col (left) -->

</div>
</section>
@endsection
