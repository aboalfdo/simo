@extends('layouts.players')

@section('content')
    <section class="content">
        @if(Session::has('alert-success'))
            <div class="alert alert-success"><i class="fa fa-check" aria-hidden="true"></i> <strong>{!! session('alert-success') !!}</strong></div>
        @endif
        @if(Session::has('alert-danger'))
            <div class="alert alert-danger"><i class="fa fa-times" aria-hidden="true"></i> <strong>{!! session('alert-danger') !!}</strong></div>
        @endif
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header col-lg-9 float-right">
                        <h3 class="box-title ">جدول القرارات : <b style="color:green"> {{$company_name}}</b></h3>
                    </div><!-- /.box-header -->
                    <div class="col-lg-3 float-left pt10">
                        <a class="btn btn-block btn-primary " href="">إضافة نسبة توزيع الأرباح</a>
                    </div>
                    <form role="form" action="{{url('')}}" method="post">
                        {{ csrf_field() }}
                        <div class="box-body">
                            <div class="form-group">
                                <label for="div">نسبة توزيع الأرباح</label>
                                <input type="text" class="form-control" name="div" id="div" placeholder="">
                                {{-- <select class="form-control" name="game_id">
                                    @foreach($games as $game)
                                        <option value="{{$game->id}}">{{$game->name}}</option>
                                    @endforeach
                                </select> --}}
                            </div>
                        </div><!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div>

            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
@endsection

@push('footer-script')
    <script>
        $(function () {

            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false
            });
        });
    </script>
@endpush


