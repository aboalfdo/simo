@extends('layouts.admin')

@section('content')

<div class="box" style="" id="current_sit">
    <div class="box-header with-border">
        <h4 style="direction: rtl" class="">
            <div class="col-lg-4 text-center">الجولة : {{$phase}} </div>
            <div class="col-lg-4 text-center">اللعبة : {{$game->name}}</div>
            <div class="col-lg-4 text-center">  محددات الوضع الجاري والمؤشرات</div>
        </h4>
    </div><!-- /.box-header -->
    <div class="box-body" id="div">
        <table id="example4" class="table table-bordered table-hover text-center">
            <thead>
            <tr>
                <th>الشركة</th>
                <th>الرصيد الجديد  X</th>
                <th>السعر الجديد لسهم  S</th>
                <th>التكاليف الثابتة العامة الجديدة  FC</th>
            </tr>
            </thead>
            <tbody>

            @forelse($properties as $property)
                <tr>
                    <td>{{$property->company->name}}</td>
                    <td>{{$property->X}}</td>
                    <td>{{$property->S}}</td>
                    <td>{{$property->FC}}</td>
                </tr>
            @empty
            @endforelse
            </tbody>
            <tfoot>     </tfoot>
        </table>
        <table id="example5" class="mt20 table table-bordered table-hover text-center">
            <thead>
            <tr>
                <th>الشركة</th>
                <th>المنتجات</th>
                <th>الطاقة الانتاجية الجديدة  Cap</th>
                <th>الكلفة المتغيرة الجديدة  UVC</th>
                <th>مؤشر السمعة النسبية  Img</th>
            </tr>
            </thead>
            <tbody>

            @forelse($parameters as $parameter)
                <tr>
                  <td>{{$parameter->company->name}}</td>

                    <td>{{$parameter->product->name}}</td>
                    <td>{{$parameter->Cap}}</td>
                    <td>{{$parameter->UVC}}</td>
                    <td>{{$parameter->Img}}</td>
                </tr>
            @empty
            @endforelse
            </tbody>
            <tfoot>     </tfoot>
        </table>
        <h4>مؤشرات السوق</h4>
        <table id="example6" class=" table table-bordered table-hover text-center">
            <thead>
            <tr>
                <th>المنتجات</th>
                <th>العرض الاجمالي  TSpl</th>
                <th>الإنفاق التسويقي الإجمالي  TMkg</th>
                <th>الإنفاق الاستثماري الإجمالي  TInv</th>
                <th>الطلب الإجمالي  TD</th>
                <th>السعر الوسطي المتداول في السوق  Avp</th>

            </tr>
            </thead>
            <tbody>

            @forelse($pointers as $pointer)
                <tr>
                    <td>{{$pointer->product->name}}</td>
                    <td>{{$pointer->TSpl}}</td>
                    <td>{{$pointer->TMkg}}</td>
                    <td>{{$pointer->TInv}}</td>
                    <td>{{$pointer->TD}}</td>
                    <td>{{$pointer->Avp}}</td>
                </tr>
            @empty
            @endforelse
            </tbody>
            <tfoot>     </tfoot>
        </table>
        <h4>الكمية والسعر</h4>

        <table id="example6" class="mt20 table table-bordered table-hover text-center">
            <thead>
            <tr>
                <th>الشركة</th>
                <th>المنتجات</th>
                <th>الكمية  FS</th>
                <th>السعر المعتمد P</th>
            </tr>
            </thead>
            <tbody>

            @foreach($quantities as $quantity)

                    <tr>
                        <td>{{$quantity->company->name}}</td>
                        <td>{{$quantity->product->name}}</td>
                        <td>{{$quantity->FS}}</td>
                        <td>
                        @for($i=0;$i<count($decisions);$i++)
                            @if($decisions[$i]->product_id==$quantity->product_id &&$decisions[$i]->user_id==$quantity->user_id)
                                {{$decisions[$i]->P}}
                            @endif
                        @endfor
                        </td>
                    </tr>

            @endforeach
            </tbody>
            <tfoot>     </tfoot>
        </table>

    </div><!-- /.box-body -->
</div><!-- /.box -->
    @if($phase==$numOfPhases)
        <div class="box" style="display: flex">
            <a type="button" href="{{url('admin/evaluate/'.$game_id)}}" id="showDiv" class="btn btn-warning btn-block" >انتهت اللعبة!! عرض تقييم الشركات</a>
        </div>
    @endif
@endsection
