@extends('layouts.admin')

@section('content')
    <section class="content">
        @if(Session::has('alert-success'))
            <div class="alert alert-success"><i class="fa fa-check" aria-hidden="true"></i> <strong>{!! Session::get('alert-success') !!}</strong></div>
        @endif
        @if(Session::has('alert-danger'))
            <div class="alert alert-danger"><i class="fa fa-times" aria-hidden="true"></i> <strong>{!! Session::get('alert-danger') !!}</strong></div>
        @endif
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h4 style="direction: rtl" class="">
                            <div class="col-lg-4 text-center">الجولة : {{$phase}} </div>
                            <div class="col-lg-4 text-center">اللعبة : {{$game->name}}</div>
                            <div class="col-lg-4 text-center"> الربح</div>
                        </h4>
                    </div><!-- /.box-header -->

                    <div class="box-body">
                        <table id="example2t" class="table table-bordered table-hover text-center">
                            <thead>
                            <tr>
                                <th>الشركة</th>
                                <th>الربح الصافي Npr</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($Npr as $Npr_)
                            <tr>
                                <td>{{$Npr_->company->name}}</td>
                                <td>{{$Npr_->Npr}}</td>
                            </tr>
                            @empty
                            @endforelse
                            </tbody>
                            <tfoot>     </tfoot>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->

                <div class="box" style="display: flex">
                    <button type="button" id="showDiv" class="btn btn-warning btn-block" >استعراض نسبة توزيع الأرباح للشركات المشاركة</button>
                </div>
                <div class="box" style="display: none" id="divTable">
                    <div class="box-body" >
                        <table id="example4" class="table table-bordered table-hover text-center">
                            <thead>
                            <tr>
                                <th>الشركة</th>
                                <th>نسبة توزيع الأرباح Div</th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($Npr as $Div)
                                <tr>
                                    <td>{{$Div->company->name}}</td>
                                    <td>{{$Div->Div_}}</td>
                                </tr>
                            @empty
                            @endforelse
                            </tbody>
                            <tfoot>     </tfoot>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->

                <div class="box" style="display: none" id="DprTable">
                    <div class="box-body" id="div">
                        <table id="example4" class="table table-bordered table-hover text-center">
                            <thead>
                            <tr>
                                <th>الشركة</th>
                                <th>الربح الموزع على حملة أسهم المنشأة Dpr</th>
                            </tr>
                            </thead>
                            <tbody>
                            @php $b=0;  @endphp
                            @forelse($Dpr as $Dpr_)
                                <tr>
                                    <td>{{$company_details[$b]->name}}</td>
                                    <td>{{$Dpr_}}</td>
                                </tr>
                                @php $b++; @endphp
                            @empty
                            @endforelse
                            </tbody>
                            <tfoot>     </tfoot>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->
                <div class="box" style="display: none" id="pointerButton">
                    <a  id="showCurrentSit" href="{{url('admin/pointers/'.$game->id.'/'.$phase)}}" class="btn btn-warning btn-block" >حساب محددات الوضع الجاري والمؤشرات</a>
                </div>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
@endsection

@push('footer-script')
    <script>
        $(function () {

            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false
            });
        });
    </script>
    <script>
        $( "#showDiv" ).click(function() {
            $('#divTable').show();
            $('#DprTable').show();
            $('#pointerButton').show();

        });
    </script>
    <script>
        $( "#showCurrentSit" ).click(function() {
            $('#current_sit').show();
        });
    </script>

@endpush
