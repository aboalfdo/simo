@extends('layouts.admin')

@section('content')
    <section class="content">
        @if(Session::has('alert-success'))
            <div class="alert alert-success"><i class="fa fa-check" aria-hidden="true"></i> <strong>{!! session('alert-success') !!}</strong></div>
        @endif
        @if(Session::has('alert-danger'))
            <div class="alert alert-danger"><i class="fa fa-times" aria-hidden="true"></i> <strong>{!! session('alert-danger') !!}</strong></div>
        @endif
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h4 style="direction: rtl" class="">
                            <div class="col-lg-4 text-center">الجولة : {{$phase}} </div>
                            <div class="col-lg-4 text-center">اللعبة : {{$game->name}}</div>
                            <div class="col-lg-4 text-center"> القرارات</div>
                        </h4>
                    </div><!-- /.box-header -->
{{--                    <div class="col-lg-3 float-left pt10">--}}
{{--                        <a class="btn btn-block btn-primary " href="{{url('decisions/create')}}">إضافة قرار</a>--}}
{{--                    </div>--}}
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover text-center">
                            <thead>
                            <tr>
                                <th>الشركة</th>
                                <th> المنتج </th>
                                <th>الكمية المصنعة في المنشأة من المنتج </th>
                                <th> السعر المعتمد في المنشأة لبيع المنتج</th>
                                <th>الإنفاق التسويقي للمنشأة على المنتج </th>
                                <th>الإنفاق الاستثماري للمنشأة على المنتج </th>
                            </tr>
                            </thead>
                            <tbody>
                            @forelse($decisions as $decision)
                            <tr>
                                <td>{{$decision->company->name}}</td>
                                <td>{{$decision->product->name}}</td>
                                <td>{{$decision->Q}}</td>
                                <td>{{$decision->P}}</td>
                                <td>{{$decision->Mkg}}</td>
                                <td>{{$decision->Inv}}</td>
{{--                                 <td>--}}
{{--                                    <div class="btn-group">--}}
{{--                                        <button type="button" class="btn btn-default">Action</button>--}}
{{--                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">--}}
{{--                                            <span class="caret"></span>--}}
{{--                                            <span class="sr-only">Toggle Dropdown</span>--}}
{{--                                        </button>--}}
{{--                                        <ul class="dropdown-menu" role="menu">--}}
{{--                                            <li><a href="#">حذف</a></li>--}}

{{--                                        </ul>--}}
{{--                                    </div>--}}
{{--                                </td>--}}

                            </tr>
                            @empty

                            @endforelse
                            </tbody>
                            <tfoot>

                            </tfoot>
                        </table>
                    </div><!-- /.box-body -->

                </div><!-- /.box -->

                <div class="box">
                    <div class="box-header with-border">
                        <h4 style="" class="">
                            <h3 class="box-title"> الشركات التي لم تدخل القرارات للجولة  {{'('.$phase.')'}}</h3>
                        </h4>
                    </div><!-- /.box-header -->

                    <div class="box-body">
                        <table id="" class="table table-bordered table-hover text-center">
                            <tbody>
                            @forelse($companiesNot as $company)
                                <tr>
                                    <td>{{$company->name}}</td>
                                </tr>
                            @empty
                                <h4 class="text-center">لايوجد شركات</h4>
                            @endforelse
                            </tbody>
                            <tfoot>
                            </tfoot>
                        </table>
                    </div><!-- /.box-body -->
                    <div class="box-footer">
                        <div class="col-lg-3 text-center">
                        </div>
                        <div class="col-lg-3 text-center">
                            <a type="submit" class="btn btn-primary btn-block" href="{{url('admin/decisions/create')}}">إدخال القرارات الافتراضية</a>
                        </div>
                        <div class="col-lg-3 text-center">
                        </div>
                        <div class="col-lg-3 text-center">
                            <a type="submit" class="btn btn-info btn-block" href="{{url('admin/game/start/'.$game->id)}}">عودة</a>
                        </div>
                    </div>
                </div>
                <!-- /.box -->
                <div class="box">
                    <div class="box-footer">
                        <div class="col-lg-12 text-center">
                            <a type="submit" class="btn btn-warning btn-block" href="{{url('admin/game/npr/'.$game->id.'/'.$phase)}}">حساب الربح الصافي للمنشآت</a>
                        </div>
                    </div>
                </div>
            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
@endsection

@push('footer-script')
    <script>
        $(function () {

            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false
            });
        });
    </script>
@endpush
