@extends('layouts.admin')

@section('content')
    <section class="content">

        <div class="row">
            <div class="col-xs-12">
                <!-- Block buttons -->
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">{{$game->name}}</h3>
                    </div>
                    <div class="box-body">
                        @for($i=0;$i<$main_parameters->phase_num;$i++)
                            @php $phase = $i+1; @endphp
                        <a type="button" class="btn btn-default btn-block  btn-lg mb-10" href="{{url('admin/game/phase/'.$game_id.'/'.$phase)}}"> الجولة   {{$phase}}  </a>
                        @endfor
                    </div>
                </div>

            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
@endsection

@push('footer-script')

@endpush
