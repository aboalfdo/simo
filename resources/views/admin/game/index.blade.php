@extends('layouts.admin')

@section('content')
    <section class="content">
        @if(Session::has('alert-success'))
            <div class="alert alert-success"><i class="fa fa-check" aria-hidden="true"></i> <strong>{!! session('alert-success') !!}</strong></div>
        @endif
        @if(Session::has('alert-danger'))
            <div class="alert alert-danger"><i class="fa fa-times" aria-hidden="true"></i> <strong>{!! session('alert-danger') !!}</strong></div>
        @endif
            @if($errors->any())
                <div class="alert alert-danger">{{$errors->first()}}</div>
            @endif
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header col-lg-9 float-right">
                        <h3 class="box-title ">جدول الألعاب</h3>
                    </div><!-- /.box-header -->
                    <div class="col-lg-3 float-left pt10">
                        <a class="btn btn-block btn-primary " href="{{url('admin-home')}}">إضافة لعبة</a>
                    </div>
                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>الاسم</th>

                                <th>Actions</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($games as $game)
                            <tr>
                                <td>{{$game->name}}</td>

                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-default">Action</button>
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                            <span class="caret"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="{{url('admin/main_parameters/create/'.$game->id)}}">أضف المحددات العامة</a></li>
                                            <li><a href="{{url('admin/products_character/create/'.$game->id)}}">أضف محددات سمات المنتجات</a></li>
                                            <li><a href="{{url('admin/main_parameters/show/'.$game->id)}}"> عرض المحددات العامة</a></li>
                                            <li><a href="{{url('admin/products_character/show/'.$game->id)}}"> عرض محددات المنتجات</a></li>
                                            <li><a href="{{url('admin/game/start/'.$game->id)}}">بدء اللعبة</a></li>
                                            <li><a href="{{url('admin/evaluate/'.$game->id)}}">التقييم</a></li>
                                            <li><a href="#">حذف</a></li>

                                        </ul>
                                    </div>
                                </td>

                            </tr>
                            @endforeach
                            </tbody>
                            <tfoot>

                            </tfoot>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->

            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
@endsection

@push('footer-script')
    <script>
        $(function () {

            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false
            });
        });
    </script>
@endpush
