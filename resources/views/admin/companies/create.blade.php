@extends('layouts.admin')

@section('content')
<section class="content">
<div class="row">
<div class="col-md-12">
    <!-- general form elements -->
    <div class="box box-primary ">
        <div class="box-header with-border">
            <h3 class="box-title">إضافة شركة</h3>
        </div><!-- /.box-header -->
        <!-- form start -->
        <form role="form" action="{{url('admin/companies/store')}}" method="post">
            {{ csrf_field() }}
            <div class="box-body">
                <div class="form-group">
                    <label for="name">الاسم</label>
                    <input type="text" name="name" class="form-control" id="name" placeholder="">
                </div>
                <div class="form-group">
                    <label for="email">الايميل</label>
                    <input type="email" name="email" class="form-control" id="email" placeholder="">
                </div>
                <div class="form-group">
                    <label for="password">كلمة السر</label>
                    <input type="password" class="form-control" name="password" id="password" placeholder="Password">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">تأكيد كلمة السر</label>
                    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="">
                </div>
                <div class="form-group">
                    <label for="game">إسناد لعبة</label>
                    <select class="form-control" name="game_id">
                        @foreach($games as $game)
                            <option value="{{$game->id}}">{{$game->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div><!-- /.box-body -->

            <div class="box-footer">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </form>
    </div><!-- /.box -->


</div><!--/.col (left) -->

</div>
</section>
@endsection
