@extends('layouts.admin')

@section('content')
<section class="content">
<div class="row">
<div class="col-md-12">
    <!-- general form elements -->
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">إضافة محددات الوضع الابتدائي لـ <b style="color:green">{{$product->name}}</b> </h3>
        </div><!-- /.box-header -->
        <!-- form start -->
        <form role="form" action="{{url('admin/first_parameters/store')}}" method="post">
            {{ csrf_field() }}
            <input type="hidden" name="product_id" value="{{$product_id}}">
            <div class="box-body">
                <div class="form-group">
                    <label for="X0">رصيد الحسابات المصرفية</label>
                    <input type="number" name="X0" class="form-control" id="X0" placeholder="2000000" value="2000000">
                </div>
                <div class="form-group">
                    <label for="FC0">التكاليف الثابتة العامة</label>
                    <input type="number" name="FC0" class="form-control" id="FC0" placeholder="2" value="2">
                </div>
                <div class="form-group">
                    <label for="S0">سعر السهم</label>
                    <input type="number" name="S0" class="form-control" id="S0" placeholder="1000" value="1000">
                </div>
                <div class="form-group">
                    <label for="Cap0">الطاقة الانتاجية في المنتج</label>
                    <input type="number" name="Cap0" class="form-control" id="Cap0" placeholder="2000" value="2000">
                </div>
                <div class="form-group">
                    <label for="UVC0">التكلفة الافرادية المتغيرة للمنتج</label>
                    <input type="number" name="UVC0" class="form-control" id="UVC0" placeholder="2" value="2">
                </div>
                <div class="form-group">
                    <label for="Img0">السمعة في المنتج</label>
                    <input type="number" name="Img0" class="form-control" id="Img0" placeholder="1" value="1" readonly>
                </div>

            <div class="box-footer">
                <button type="submit" class="btn btn-block btn-primary">حفظ</button>
            </div>
        </form>
    </div><!-- /.box -->


</div><!--/.col (left) -->

</div>
</section>
@endsection
