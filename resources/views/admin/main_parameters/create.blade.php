@extends('layouts.admin')

@section('content')
    <section class="content">

        <div class="row">
            <div class="col-md-12">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">إضافة المحددات العامة لـ <b style="color:green">{{$game->name}}</b> </h3>
                    </div><!-- /.box-header -->
                    <!-- form start -->
                    <form role="form" id="main_params" action="{{url('admin/main_parameters/store')}}" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" name="game_id" value="{{$game_id}}">
                        <div class="box-body">
                            <div class="form-group">
                                <label for="N">عدد المنشآت المتنافسة</label>
                                <input type="number" name="N" class="form-control" id="N" placeholder="10" value="10">
                            </div>
                            <div class="form-group">
                                <label for="M">عدد المنتجات المطروحة</label>
                                <input type="number" name="M" class="form-control" id="M" placeholder="2" value="2">
                            </div>
                            <div class="form-group">
                                <label for="phase_num">عدد الجولات المقررة:</label>
                                <input type="number" name="phase_num" class="form-control" id="phase_num" placeholder="3" value="3">
                            </div>
                            <div class="form-group">
                                <label for="T">معدل الضريبة على الأرباح</label>
                                <select class="form-control" name="T">
                                    <option value="0.2">منخفض </option>
                                    <option value="0.3" selected>معتدل </option>
                                    <option value="0.4">مرتفع </option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="R">معدل الفائدة على الانكشاف</label>
                                <select class="form-control" name="R">
                                    <option value="0.15">منخفض </option>
                                    <option value="0.2" selected>معتدل </option>
                                    <option value="0.25">مرتفع </option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="tau">معامل ضبط الانكشاف</label>
                                <select class="form-control" name="tau">
                                    <option value="0.6">تحصيل سريع </option>
                                    <option value="0.5" selected>تحصيل معتدل السرعة </option>
                                    <option value="0.4">تحصيل بطيء</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="alpha">المدة  القانونية لاحتساب الاهتلاك</label>
                                <select class="form-control" name="alpha">
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5" selected>5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="gamma">معامل تأثر سعر السهم بالربح المحقق</label>
                                <select class="form-control" name="gamma">
                                    <option value="0.2">تأثر ضعيف </option>
                                    <option value="0.3" selected>تأثر معتدل  </option>
                                    <option value="0.4">تأثر شديد </option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="beta">معامل تأثر سعر السهم بالربح الموّزع</label>
                                <select class="form-control" name="beta">
                                    <option value="0.45">تأثر ضعيف </option>
                                    <option value="0.6" selected>تأثر معتدل  </option>
                                    <option value="0.75">تأثر شديد </option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="omega">معامل تأثر السمعة بالإنفاق التسويقي ال

                                    جاري</label>
                                <select class="form-control" name="omega">
                                    <option value="0.4">تأثر ضعيف </option>
                                    <option value="0.5" selected>تأثر معتدل </option>
                                    <option value="0.6">تأثر شديد </option>
                                </select>
                            </div>
                            <hr>
                            <div class="alert alert-danger" id="msgdiv" style="display: none"><i class="fa fa-times" aria-hidden="true"></i> <strong id="msg"></strong></div>
                            <div class="box-header with-border">
                                <h3 class="box-title">محددات الوضع الابتدائي</h3>
                            </div><!-- /.box-header -->

                            <div class="form-group">
                                <label for="X0">رصيد الحسابات المصرفية</label>
                                <input type="number" name="X0" class="form-control" id="X0" placeholder="2000000" value="2000000">
                            </div>
                            <div class="form-group">
                                <label for="FC0">التكاليف الثابتة العامة</label>
                                <input type="number" name="FC0" class="form-control" id="FC0" placeholder="2" value="2">
                            </div>
                            <div class="form-group">
                                <label for="S0">سعر السهم</label>
                                <input type="number" name="S0" class="form-control" id="S0" placeholder="1000" value="1000">
                            </div>

                        </div><!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">حفظ</button>
                        </div>
                    </form>
                </div><!-- /.box -->

            </div><!--/.col (left) -->

        </div>
    </section>
@endsection

@push('footer-script')

<script>
    $( "#main_params" ).submit(function( event ) {
       X0  = $( "#X0" ).val();
        FC0  = $( "#FC0" ).val();
        left = Math.round(X0/6);
        right = Math.round(X0/4);

        if(FC0>X0/4 || FC0<X0/6)
        {
            $("#msgdiv").show();
            var s = 'يجب أن تكون قيمة التكاليف الثابتة العامة أصغر من ' ;
            s +=right;
            s += ' وأكبر من ';
            s += left;
            $("#msg").html(s);

            event.preventDefault();
        }
        // alert(X0  );
        // event.preventDefault();
    });
</script>

@endpush
