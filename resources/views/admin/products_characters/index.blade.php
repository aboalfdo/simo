@extends('layouts.admin')

@section('content')
    <section class="content">
        @if(Session::has('alert-success'))
            <div class="alert alert-success"><i class="fa fa-check" aria-hidden="true"></i> <strong>{!! Session::get('alert-success') !!}</strong></div>
        @endif
        @if(Session::has('alert-danger'))
            <div class="alert alert-danger"><i class="fa fa-times" aria-hidden="true"></i> <strong>{!! Session::get('alert-danger') !!}</strong></div>
        @endif
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header col-lg-9 float-right">
                        <h3 class="box-title ">عرض المنتجات المعرفة ضمن اللعبة مع محدداتها</h3>
                    </div><!-- /.box-header -->
                    <div class="col-lg-3 float-left pt10">
                        <a class="btn btn-block btn-primary " href="{{ url('admin/products_character/create/{game_id}') }}">إضافة  منتج جديد</a>
                    </div>

                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                    <tr>
                        <th>
                            اسم المنتج
                        </th>
                        <th>
                            معدل نمو الطلب الاجمالي للمنتج
                        </th>
                        <th>
                            مرونة الطلب الوسطية للمنتج
                        </th>
                        <th>
                            معدل الاسترداد من قيمة فائض المنتج
                        </th>
                        <th>
                            معامل فاعلية السمعة في المنتج
                     </th>
                        <th>
                            معامل  التذبذب في الطلب على المنتج
                        </th>
                        <th>
                            معامل الاستيعاب الابتدائي للمنتج
                        </th>
                        <th class="text-center">
                            تعديل / حذف منتج
                        </th>
                    </tr>
                </thead>

                <tbody>
                    @foreach ($products as $product)
                        <tr class="text-center">

                            <td>
                                <a
                                href="{{ url('admin/products_character/show', $product) }}">{{ $product->name }}</a>
                            </td>

                            <td>
                                {{ $product->Gr }}
                            </td>
                            <td>
                                {{ $product->epsilon }}
                            </td>
                            <td>
                                {{ $product->pbv }}
                            </td>
                            <td>
                                {{ $product->rho }}
                            </td>
                            <td>
                                {{ $product->fl }}
                            </td>
                            <td>
                                {{ $product->sz }}
                            </td>
                            <td>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default">Action</button>
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                        <span class="caret"></span>
                                        <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <ul class="dropdown-menu" role="menu">
                                        <li> <a href="{{ url('admin/products_character/destroy', $product->id) }}"> حذف</a>
                                        </li>
                                        <li><a href="{{ url('admin/products_character/edit' ,$product) }}">تعديل</a>
                                        </li>

                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
                <tfoot>

                </tfoot>
            </table>
    </div><!-- /.box -->


</div><!--/.col (left) -->

</div>
</section>
@endsection

@push('footer-script')
    <script>
        $(function () {

            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false
            });
        });
    </script>
@endpush

