@extends('layouts.admin')

@section('content')
<section class="content">
<div class="row">
<div class="col-md-12">
    <!-- general form elements -->
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">تعديل المنتج {{$product->name}}  </h3>
        </div><!-- /.box-header -->
        <!-- form start -->

        <form role="form" action="{{url('admin/products_character/update' , $product->id)}}" method="POST">
        @method('PUT')
        @csrf

            <div class="box-body">

                <div class="form-group">
                    <label for="name">اسم المنتج </label>
                    <input type="text" name="name" class="form-control" placeholder="" value="{{$product->name}}">
                </div>
                <div class="form-group">
                    <label for="gr">معدل نمو الطلب الاجمالي للمنتج </label>
                    <input type="number" name="gr" class="form-control" step="0.0001" placeholder="0.05" value="{{$product->gr}}">
                </div>
                <div class="form-group">
                    <label for="epsilon">مرونة الطلب الوسطية للمنتج  </label>
                    <input type="number" name="epsilon" class="form-control" step="0.0001" placeholder="1.2" value="{{$product->epsilon}}">
                </div>
                <div class="form-group">
                    <label for="pbv">معدل الاسترداد من قيمة فائض المنتج </label>
                    <select class="form-control" name="pbv" >
                        <option value="0.00">استرداد معدوم</option>
                        <option value="0.25" selected>استرداد بسيط</option>
                        <option value="0.5">استرداد مهم</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="rho">معامل فاعلية السمعة في المنتج </label>
                    <select class="form-control" name="rho">
                        <option value="0.15">فاعلية ضعيفة</option>
                        <option value="0.25" selected>فاعلية معتدلة </option>
                        <option value="0.35">فاعلية شديدة </option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="fl">معامل  التذبذب في الطلب على المنتج </label>
                    <select class="form-control" name="fl">
                        <option value="0.01">تذبذب بسيط</option>
                        <option value="0.02" selected>تذبذب معتدل</option>
                        <option value="0.03">تذبذب مهم</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="sz">معامل الاستيعاب الابتدائي للمنتج </label>
                    <select class="form-control" name="sz">
                        <option value="0.9">منافسة ضعيفة </option>
                        <option value="0.75" selected>منافسة معتدلة</option>
                        <option value="0.6">منافسة شديدة </option>
                    </select>
                </div>

            </div><!-- /.box-body -->

            <div class="box-footer">
                <button type="submit" class="btn btn-primary">تعديل</button>
            </div>
        </form>

    </div><!-- /.box -->


</div><!--/.col (left) -->

</div>
</section>
@endsection
