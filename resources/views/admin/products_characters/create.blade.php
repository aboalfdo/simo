@extends('layouts.admin')

@section('content')
    <div class="alert alert-danger" id="msgdiv" style="display: none"><i class="fa fa-times" aria-hidden="true"></i> <strong id="msg"></strong></div>
<section class="content">
<div class="row">
<div class="col-md-12">
    <!-- general form elements -->
    <div class="box box-primary">
        <div class="box-header with-border">
            <h3 class="box-title">إضافة منتجات جديدة مع محدداتها   </h3>
        </div><!-- /.box-header -->
        <!-- form start -->

        <form role="form" id="first_chars" action="{{url('admin/products_character/store')}}" method="post">
            {{ csrf_field() }}
            <input type="hidden" name="game_id" value="{{$game_id}}">

            @for($i=0;$i<$main_parameters->M;$i++)

            <div class="box-body">
                <div class="form-group">
                    <h4 style="font-style: italic">المنتج {{$i+1}}</h4>
                </div>
                <div class="form-group">
                    <label for="name">اسم المنتج </label>
                    <input type="text" name="name[]" class="form-control" placeholder="" value="" required>
                </div>
                <div class="form-group">
                    <label for="gr">معدل نمو الطلب الاجمالي للمنتج </label>
                    <input type="number" name="gr[]" class="form-control" step="0.0001" placeholder="0.05" value="0.05" required>
                </div>
                <div class="form-group">
                    <label for="epsilon">مرونة الطلب الوسطية للمنتج  </label>
                    <input type="number" name="epsilon[]" class="form-control" step="0.0001" placeholder="1.2" value="1.2" required>
                </div>
                <div class="form-group">
                    <label for="pbv">معدل الاسترداد من قيمة فائض المنتج </label>
                    <select class="form-control" name="pbv[]" required>
                        <option value="0.00">استرداد معدوم</option>
                        <option value="0.25" selected>استرداد بسيط</option>
                        <option value="0.5">استرداد مهم</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="rho">معامل فاعلية السمعة في المنتج </label>
                    <select class="form-control" name="rho[]" required>
                        <option value="0.15">فاعلية ضعيفة</option>
                        <option value="0.25" selected>فاعلية معتدلة </option>
                        <option value="0.35">فاعلية شديدة </option>
                    </select>
                </div>

                <div class="form-group">
                    <label for="fl">معامل  التذبذب في الطلب على المنتج </label>
                    <select class="form-control" name="fl[]" required>
                        <option value="0.01">تذبذب بسيط</option>
                        <option value="0.02" selected>تذبذب معتدل</option>
                        <option value="0.03">تذبذب مهم</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="sz">معامل الاستيعاب الابتدائي للمنتج </label>
                    <select class="form-control" name="sz[]" required>
                        <option value="0.9">منافسة ضعيفة </option>
                        <option value="0.75" selected>منافسة معتدلة</option>
                        <option value="0.6">منافسة شديدة </option>
                    </select>
                </div>
            <hr>

                <div class="form-group">
                    <label for="Cap0">الطاقة الانتاجية في المنتج</label>
                    <input type="number" name="Cap0" class="form-control Cap0" id="Cap0" placeholder="2000" value="2000">
                </div>
                <div class="form-group">
                    <label for="UVC0">التكلفة الافرادية المتغيرة للمنتج</label>
                    <input type="number" name="UVC0" class="form-control UVC0" id="UVC0" placeholder="2" value="2">
                </div>
                <div class="form-group">
                    <label for="Img0">السمعة في المنتج</label>
                    <input type="number" name="Img0" class="form-control" id="Img0" placeholder="1" value="1" readonly>
                </div>


            </div><!-- /.box-body -->
                @endfor
            <input value="{{$main_parameters->M}}" id="m" type="hidden">
            <input value="{{$main_parameters->X0}}" id="X0" type="hidden">
            <div class="box-footer">
                <button type="submit" class="btn btn-primary">حفظ</button>
            </div>
        </form>

    </div><!-- /.box -->


</div><!--/.col (left) -->

</div>
</section>
@endsection

@push('footer-script')

    <script>
        $( "#first_chars" ).submit(function( event ) {
            var Cap0 = $(".Cap0");
            var UVC0 = $(".UVC0");
            for(var i = 0; i < Cap0.length; i++){
                Cap = $(Cap0[i]).val();
                UVC = $(UVC0[i]).val();
              // alert($(Cap0[i]).val());
                m  = $( "#m" ).val();
                X0 = $( "#X0" ).val();
                left = 0.7*X0/(m*Cap);
                right = 0.9*X0/(m*Cap);
                if(UVC>right || UVC<left)
                {
                    $("#msgdiv").show();
                    var s = 'يجب أن تكون قيمة التكلفة الافرادية المتغيرة أصغر من ' ;
                    s +=right;
                    s += ' وأكبر من ';
                    s += left;
                    $("#msg").html(s);

                    event.preventDefault();
                }
            }
            // m  = $( "#m" ).val();
            // Cap0  = $( ".Cap0" ).val();
            // UVC0  = $( ".UVC0" ).val();
            // X0 = $( "#X0" ).val();
            // left = 0.7*X0/(m*Cap0);
            // right = Math.round(X0/4);
            //
            // if(FC0>X0/4 || FC0<X0/6)
            // {
            //     $("#msgdiv").show();
            //     var s = 'يجب أن تكون قيمة التكاليف الثابتة العامة أصغر من ' ;
            //     s +=right;
            //     s += ' وأكبر من ';
            //     s += left;
            //     $("#msg").html(s);
            //
            //     event.preventDefault();
            // }
           // alert(Cap0 );
            // event.preventDefault();
        });
    </script>
@endpush
