@extends('layouts.admin')

@section('content')
    <section class="content">
        @if(Session::has('alert-success'))
            <div class="alert alert-success"><i class="fa fa-check" aria-hidden="true"></i> <strong>{!! session('alert-success') !!}</strong></div>
        @endif
        @if(Session::has('alert-danger'))
            <div class="alert alert-danger"><i class="fa fa-times" aria-hidden="true"></i> <strong>{!! session('alert-danger') !!}</strong></div>
        @endif
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header col-lg-9 float-right">
                        <h3 class="box-title ">محددات المنتجات للعبة {{$game->name}}</h3>
                    </div>
                   <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                    <tr>
                        <th>
                            اسم المنتج
                        </th>
                        <th>
                            معدل نمو الطلب الاجمالي للمنتج
                        </th>
                        <th>
                            مرونة الطلب الوسطية للمنتج
                        </th>
                        <th>
                            معدل الاسترداد من قيمة فائض المنتج
                        </th>
                        <th>
                            معامل فاعلية السمعة في المنتج
                     </th>
                        <th>
                            معامل  التذبذب في الطلب على المنتج
                        </th>
                        <th>
                            معامل الاستيعاب الابتدائي للمنتج
                        </th>
                        <th class="text-center">
                            تعديل / حذف منتج
                        </th>
                    </tr>
                </thead>

                <tbody>

                            <tr>
                                <td>  {{ $productCharacters->name }}</td>
                                <td>  {{ $productCharacters->Gr }}</td>
                                <td>  {{ $productCharacters->epsilon }}</td>
                                <td>  {{ $productCharacters->pbv }}</td>
                                <td>{{ $productCharacters->rho }}</td>
                                <td>{{ $productCharacters->fl }}</td>
                                <td> {{ $productCharacters->sz }}</td>
                                <td>
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-default">Action</button>
                                        <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                            <span class="caret"></span>
                                            <span class="sr-only">Toggle Dropdown</span>
                                        </button>
                                        <ul class="dropdown-menu" role="menu">
                                            <li><a href="#">حذف</a></li>

                                        </ul>
                                    </div>
                                </td>
                            </tr>

                            </tbody>
                            <tfoot>

                            </tfoot>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->

            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
@endsection

@push('footer-script')
    <script>
        $(function () {

            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false
            });
        });
    </script>
@endpush
