@extends('layouts.admin')

@section('content')

    <!-- Content Header (Page header) -->
   <section class="content">

            @if(Session::has('alert-success'))
                <div class="alert alert-success"><i class="fa fa-check" aria-hidden="true"></i> <strong>{!! session('alert-success') !!}</strong></div>
            @endif
            @if(Session::has('alert-danger'))
                <div class="alert alert-danger"><i class="fa fa-times" aria-hidden="true"></i> <strong>{!! session('alert-danger') !!}</strong></div>
            @endif

            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">إضافة لعبة جديدة</h3>
                </div><!-- /.box-header -->
                <!-- form start -->
                <form role="form" action="{{url('game/store')}}" method="post">
                    {{ csrf_field() }}
                    <div class="box-body">
                        <div class="form-group">
                            <label for="game">اسم اللعبة</label>
                            <input type="text" class="form-control" id="game" name="game" placeholder="أدخل الاسم">
                        </div>
                    </div><!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">حفظ</button>
                    </div>
                </form>
            </div><!-- /.box -->

    </section><!-- /.content -->
@endsection
