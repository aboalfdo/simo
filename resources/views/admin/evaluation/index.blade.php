@extends('layouts.admin')

@section('content')
    <section class="content">
        @if(Session::has('alert-success'))
            <div class="alert alert-success"><i class="fa fa-check" aria-hidden="true"></i> <strong>{!! Session::get('alert-success') !!}</strong></div>
        @endif
        @if(Session::has('alert-danger'))
            <div class="alert alert-danger"><i class="fa fa-times" aria-hidden="true"></i> <strong>{!! Session::get('alert-danger') !!}</strong></div>
        @endif
            @if($errors->any())
                <div class="alert alert-danger">{{$errors->first()}}</div>
            @endif
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header col-lg-9 float-right">
                        <h4 style="direction: rtl" class="">

                            <div class="col-lg-6 text-center">اللعبة : {{$game->name}}</div>
                            <div class="col-lg-6 text-center"> التقييم</div>
                        </h4>
                    </div><!-- /.box-header -->

                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <th>الشركة</th>
                                <th>Perf</th>
                                <th>Comp</th>
                                <th>Scor</th>

                            </tr>
                            </thead>
                            <tbody>
                            @php $i=0; @endphp
                            @foreach($companies as $company)
                            <tr>

                                <td>{{$company->company->name}}</td>
                                <td>{{$perf[$company->user_id]}}</td>
                                <td>{{$Comp[$company->user_id]}}</td>
                                <td>{{$Scor[$company->user_id]}}</td>

                            </tr>
                                @php $i++;@endphp
                            @endforeach
                            </tbody>
                            <tfoot>

                            </tfoot>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->

            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
@endsection

@push('footer-script')
    <script>
        $(function () {

            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false
            });
        });
    </script>
@endpush
