@extends('layouts.players')

@section('content')
    <section class="content">
        @if(Session::has('alert-success'))
            <div class="alert alert-success"><i class="fa fa-check" aria-hidden="true"></i> <strong>{!! session('alert-success') !!}</strong></div>
        @endif
        @if(Session::has('alert-danger'))
            <div class="alert alert-danger"><i class="fa fa-times" aria-hidden="true"></i> <strong>{!! session('alert-danger') !!}</strong></div>
        @endif
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h4 style="direction: rtl" class="">
                            <div class="col-lg-3 text-center">الجولة : {{$phase}} </div>
                            <div class="col-lg-3 text-center">اللعبة : {{$game->name}}</div>
                            <div class="col-lg-3 text-center"> الربح</div>
                            <div class="col-lg-3 text-center">الشركة {{$company_name}}</div>
                        </h4>
                    </div><!-- /.box-header -->

                    <div class="box-body">
                        <table id="example2t" class="table table-bordered table-hover text-center">
                            <thead>
                            <tr>

                                <th>الربح الصافي Npr</th>
                                <th>نسبة توزيع الارباح Div</th>
                                <th>الأرباح الموزعة Dpr</th>
                            </tr>
                            </thead>
                            <tbody>

                            <tr>
                                <td>{{$dpr->Npr}}</td>
                                <td>{{$dpr->Div_}}</td>
                                <td>{{$dpr->Dpr}}</td>
                            </tr>

                            </tbody>
                            <tfoot>     </tfoot>
                        </table>
                    </div><!-- /.box-body -->
                </div><!-- /.box -->

                <div class="box" style="display: flex">
                    <button type="button" id="showDiv" class="btn btn-warning btn-block" >استعراض نسبة توزيع الأرباح للشركات المشاركة</button>
                </div>

            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
@endsection

@push('footer-script')
    <script>
        $(function () {

            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false
            });
        });
    </script>
    <script>
        $( "#showDiv" ).click(function() {
            $('#divTable').show();
            $('#DprTable').show();
            $('#pointerButton').show();

        });
    </script>
    <script>
        $( "#showCurrentSit" ).click(function() {
            $('#current_sit').show();
        });
    </script>

@endpush
