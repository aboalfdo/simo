@extends('layouts.players')

@section('content')
    <section class="content">
        @if(Session::has('alert-success'))
            <div class="alert alert-success"><i class="fa fa-check" aria-hidden="true"></i> <strong>{!! session('alert-success') !!}</strong></div>
        @endif
        @if(Session::has('alert-danger'))
            <div class="alert alert-danger"><i class="fa fa-times" aria-hidden="true"></i> <strong>{!! session('alert-danger') !!}</strong></div>
        @endif
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h4 style="direction: rtl" class="">

                            <div class="col-lg-3 text-center">الشركة : {{$company_name}} </div>
                            <div class="col-lg-3 text-center">الجولة : {{$phase}} </div>
                            <div class="col-lg-3 text-center">اللعبة : {{$game->name}}</div>
                            <div class="col-lg-3 text-center"> الربح</div>
                        </h4>
                    </div><!-- /.box-header -->

                    <div class="box-body">
                        <table id="example2" class="table table-bordered table-striped table-hover text-center">
                            <thead>
                            </thead>
                            <tbody>
                            @if($Npr)
                            <tr>
                                <th class="col-lg-6">الربح الصافي Npr</th>
                                <td>{{$Npr->Npr}}</td>
                            </tr>
                            @else
                            <tr>
                                <th colspan="2">لاتوجد بيانات</th>
                            </tr>
                            @endif
                            </tbody>

                            <tfoot>
                            </tfoot>
                        </table>
                    </div><!-- /.box-body -->
                    <div class="box-footer">
                        <div class="col-lg-6 text-center">
                        </div>


                        <div class="col-lg-6 text-center">
                            <a type="submit" class="btn btn-info btn-block" href="{{url('companies/game')}}">عودة</a>
                        </div>
                    </div>

                </div><!-- /.box -->
                @if($Npr)
                <div class="box">
                    <div class="box-header col-lg-12  float-right">
                        <h3 class="box-title ">إضافة نسبة توزيع الأرباح : <b style="color:green"> </b></h3>
                    </div><!-- /.box-header -->

                    <form role="form" action="{{url('/div')}}" method="post">
                        {{ csrf_field() }}
                        <div class="box-body">
                            <div class="form-group">
                                <label for="div">نسبة توزيع الأرباح</label>
                                <input type="number" class="form-control" step="0.01" name="div" id="div" placeholder="">
                                <input type="hidden" name="phase" value="{{$phase}}">
                            </div>
                        </div><!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">إدخال</button>
                        </div>
                    </form>
                </div>
                @endif


            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
@endsection

@push('footer-script')
    <script>
        $(function () {

            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": false
            });
        });
    </script>
@endpush
