@extends('layouts.players')

@section('content')
    <section class="content">
        @if(Session::has('alert-success'))
            <div class="alert alert-success"><i class="fa fa-check" aria-hidden="true"></i> <strong>{!! session('alert-success') !!}</strong></div>
        @endif
        @if(Session::has('alert-danger'))
            <div class="alert alert-danger"><i class="fa fa-times" aria-hidden="true"></i> <strong>{!! session('alert-danger') !!}</strong></div>
        @endif
        @if($errors->any())
            <div class="alert alert-danger">{{$errors->first()}}</div>
        @endif
        <div class="row">
            <div class="col-xs-12">
                <!-- Block buttons -->
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title">{{$game->name}}</h3>
                    </div>
                    <div class="box-body">
                        @for($i=0;$i<$main_parameters->phase_num;$i++)
                            @php $phase = $i+1; @endphp
                            <a type="button" class="btn btn-default btn-block  btn-lg mb-10" href="{{url('div/'.$phase)}}"> الجولة   {{$phase}}  </a>
                        @endfor
                    </div>
                </div>

            </div><!-- /.col -->
        </div><!-- /.row -->
    </section><!-- /.content -->
@endsection

@push('footer-script')

@endpush
