<?php

use App\Http\Controllers\AdminProductCharacterController;
use App\Http\Controllers\AdminDefaultDecisionController;
use App\Http\Controllers\FunctionsController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});


Auth::routes();

    Route::get('/home', 'HomeController@index')->name('home');

    //login
    Route::get('/admin-login', 'AdminLoginController@login');
    Route::post('/admin-login', 'AdminLoginController@validation')->name('admin.login');
    Route::get('/admin-logout', 'AdminLoginController@logout')->name('admin.logout');

    Route::get('/admin-home', 'HomeController@home');
    Route::post('game/store', 'HomeController@store');
    Route::get('admin/game', 'HomeController@index');
    Route::get('admin/game/start/{gameId}', 'HomeController@start');
    Route::get('admin/game/phase/{gameId}/{phase}', 'HomeController@phase');
    Route::get('admin/game/npr/{gameId}/{phase}', 'HomeController@caculate_Npr');
    Route::get('admin/shownpr/{gameId}/{phase}', 'HomeController@show_Npr');
    Route::get('admin/market_pointers/{gameId}/{phase}', 'HomeController@market_pointers');

    Route::get('admin/pointers/{gameId}/{phase}', 'AdminPointersController@index');
    //companies
    Route::get('admin/companies', 'AdminCompanyController@index');
    Route::get('admin/companies/create', 'AdminCompanyController@create');
    Route::post('admin/companies/store', 'AdminCompanyController@store');

    //main parameters
    Route::get('admin/main_parameters', 'AdminMainParametersController@index');
    Route::get('admin/main_parameters/create/{game_id}', 'AdminMainParametersController@create');
    Route::post('admin/main_parameters/store', 'AdminMainParametersController@store');
    Route::get('admin/main_parameters/show/{game_id}', 'AdminMainParametersController@show');

    //first parameters
    Route::get('admin/first_parameters/create/{product_id}', 'AdminFirstParamtersController@create');
    Route::post('admin/first_parameters/store', 'AdminFirstParamtersController@store');
    Route::get('admin/first_parameters', 'AdminFirstParamtersController@index');
    //Products characters
    Route::get('admin/products_character/create/{game_id}', 'AdminProductCharacterController@create');
    Route::post('admin/products_character/store', 'AdminProductCharacterController@store');
    Route::get('admin/products_character/', 'AdminProductCharacterController@index');
    Route::get('admin/products_character/update/{id}', 'AdminProductCharacterController@update');
    Route::get('admin/products_character/show/{game_id}', 'AdminProductCharacterController@show');
    Route::get('admin/decisions/create', [AdminDefaultDecisionController::class,'create']);
    Route::post('admin/decisions/store', [AdminDefaultDecisionController::class,'store']);

    Route::get('admin/products_character/edit/{id}', [AdminProductCharacterController::class,'edit' ]);
    Route::post('admin/products_character/edit/{id}', [AdminProductCharacterController::class,'edit' ]);

    //Route::resource('products', AdminProductCharacterController::class);
    Route::get('admin/test', [FunctionsController::class,'test' ]);
    Route::get('admin/continue_calculations', 'HomeController@continue_calculations');

    Route::get('admin/evaluate/{id}', 'EvaluationController@index');
//    Route::get('admin/evaluate/{id}/{phase}', 'EvaluationController@index');
//    Route::get('admin/evaluate_param/{game_id}/{phase}', 'EvaluationController@evaluations_parameters');

    //companies
    //start-game
    Route::get('companies/game', 'DecisionsController@start');
    //Route::get('game/phase/', 'DecisionsController@phase');

    Route::get('/decisions', 'DecisionsController@index');
    Route::get('/decisions/create', 'DecisionsController@create');
    Route::post('/decisions/store', 'DecisionsController@store');
    Route::get('/div/{phase}', 'DecisionsController@insert_div');
    Route::post('/div', 'DecisionsController@div');
    Route::get('/pointersComp/{phase}', 'DecisionsController@pointers');
    Route::get('/dprComp/{phase}', 'DecisionsController@dpr');

    Route::get('/market_pointers/{game}/{phase}', 'AdminPointersController@market_pointers');




