<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFirstParamtersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('first_paramters', function (Blueprint $table) {
            $table->id();
            $table->integer('X0');
            $table->integer('FC0');
            $table->integer('S0');
            $table->integer('Cap0');
            $table->integer('UVC0');
            $table->integer('Img0');
            $table->integer('product_id');
            //$table->integer('game_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('first_paramters');
    }
}
