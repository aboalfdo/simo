<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMainParametersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('main_parameters', function (Blueprint $table) {
            $table->id();
            $table->integer('N');
            $table->integer('M');
            $table->integer('T');
            $table->float('R');
            $table->float('tau');
            $table->integer('alpha');
            $table->float('gamma');
            $table->float('beta');
            $table->float('omega');
            $table->float('lambda')->default(0.5);
            $table->float('mu')->default(1.5);
            $table->float('theta')->default(2.5);
            $table->integer('delta')->default(1);
            $table->float('eta')->default(0.5);
            $table->integer('game_id');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('main_parameters');
    }
}
