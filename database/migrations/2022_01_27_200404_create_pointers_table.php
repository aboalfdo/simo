<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePointersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
//        array_push($Tspl, $Tspl_i);
//        array_push($Tmkg, $TMkg_i);
//        array_push($TInv, $TInv_i);
//        array_push($TD, $TD_i);
//        array_push($Avp, $Avp_i);
        Schema::create('pointers', function (Blueprint $table) {
            $table->id();
            $table->float('TSpl');
            $table->float('TMkg');
            $table->float('TInv');
            $table->float('TD');
            $table->float('Avp');
            $table->integer('phase');
            $table->integer('product_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pointers');
    }
}
