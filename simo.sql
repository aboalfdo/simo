-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Feb 01, 2022 at 05:39 PM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.2.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `simo`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` int(10) UNSIGNED NOT NULL,
  `email` varchar(191) NOT NULL,
  `password` varchar(191) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `remember_token` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `email`, `password`, `created_at`, `updated_at`, `remember_token`) VALUES
(1, 'admin@admin.com', '$2y$10$nbuZHYYAS9hZGOeAuswUB.pxSdRg3EW54MrFsT5p7W/hYNp8ah1xy', NULL, NULL, 'PDsdfupoiGavPCcinEQtTKVIxvuYQd5b3K5XS1zBrjFVkZmyIbWuJu1IOEyn');

-- --------------------------------------------------------

--
-- Table structure for table `company_game`
--

CREATE TABLE `company_game` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `game_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `company_game`
--

INSERT INTO `company_game` (`id`, `user_id`, `game_id`, `created_at`, `updated_at`) VALUES
(1, 3, 2, '2021-11-15 12:02:02', '2021-11-15 12:02:02'),
(2, 4, 2, '2021-11-20 11:18:33', '2021-11-20 11:18:33'),
(3, 6, 6, '2022-01-12 21:11:12', '2022-01-12 21:11:12'),
(4, 7, 6, '2022-01-12 21:11:46', '2022-01-12 21:11:46'),
(5, 8, 7, '2022-01-29 11:18:52', '2022-01-29 11:18:52'),
(6, 9, 7, '2022-01-29 11:19:58', '2022-01-29 11:19:58'),
(7, 10, 7, '2022-01-29 11:20:29', '2022-01-29 11:20:29');

-- --------------------------------------------------------

--
-- Table structure for table `decisions`
--

CREATE TABLE `decisions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `Q` int(11) NOT NULL,
  `P` double(8,2) NOT NULL,
  `Mkg` int(11) NOT NULL,
  `Inv` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `phase` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `decisions`
--

INSERT INTO `decisions` (`id`, `Q`, `P`, `Mkg`, `Inv`, `product_id`, `phase`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 50, 55.00, 5, 5, 1, 1, 4, NULL, NULL),
(2, 60, 40.00, 7, 4, 2, 1, 4, NULL, NULL),
(5, 50, 100.00, 40, 30, 1, 2, 4, '2021-12-21 21:37:26', '2021-12-21 21:37:26'),
(6, 51, 1011.00, 41, 31, 2, 2, 4, '2021-12-21 21:37:26', '2021-12-21 21:37:26'),
(9, 50, 55.00, 4, 5, 1, 1, 3, NULL, NULL),
(10, 61, 41.00, 7, 4, 2, 1, 3, NULL, NULL),
(11, 50, 100.00, 40, 30, 1, 2, 3, '2022-01-21 22:00:20', '2022-01-21 22:00:20'),
(12, 51, 101.00, 40, 30, 2, 2, 3, '2022-01-21 22:00:20', '2022-01-21 22:00:20'),
(13, 55, 100.00, 40, 30, 1, 3, 4, '2022-01-21 22:12:29', '2022-01-21 22:12:29'),
(14, 55, 200.00, 40, 30, 2, 3, 4, '2022-01-21 22:12:29', '2022-01-21 22:12:29'),
(15, 75, 100.00, 10, 10, 1, 3, 3, '2022-01-21 22:14:36', '2022-01-21 22:14:36'),
(16, 50, 50.00, 10, 5, 2, 3, 3, '2022-01-21 22:14:36', '2022-01-21 22:14:36'),
(17, 1000, 500.00, 50000, 100000, 9, 1, 8, '2022-01-29 11:23:45', '2022-01-29 11:23:45'),
(18, 1300, 600.00, 60000, 80000, 10, 1, 8, '2022-01-29 11:23:45', '2022-01-29 11:23:45'),
(19, 1200, 800.00, 100000, 50000, 9, 1, 9, '2022-01-29 11:25:35', '2022-01-29 11:25:35'),
(20, 1300, 500.00, 60000, 500000, 10, 1, 9, '2022-01-29 11:25:35', '2022-01-29 11:25:35'),
(21, 1400, 500.00, 50000, 70000, 9, 1, 10, '2022-01-29 11:26:34', '2022-01-29 11:26:34'),
(22, 1300, 800.00, 50000, 100000, 10, 1, 10, '2022-01-29 11:26:34', '2022-01-29 11:26:34'),
(23, 1000, 500.00, 50000, 50000, 9, 2, 8, '2022-02-01 13:45:20', '2022-02-01 13:45:20'),
(24, 1200, 800.00, 60000, 50000, 10, 2, 8, '2022-02-01 13:45:20', '2022-02-01 13:45:20'),
(25, 1150, 550.00, 55000, 75000, 9, 2, 9, '2022-02-01 13:49:34', '2022-02-01 13:49:34'),
(26, 1000, 740.00, 60000, 85000, 10, 2, 9, '2022-02-01 13:49:34', '2022-02-01 13:49:34'),
(27, 1100, 800.00, 50000, 60000, 9, 2, 10, '2022-02-01 13:50:37', '2022-02-01 13:50:37'),
(28, 1500, 500.00, 50000, 85000, 10, 2, 10, '2022-02-01 13:50:37', '2022-02-01 13:50:37');

-- --------------------------------------------------------

--
-- Table structure for table `evaluation`
--

CREATE TABLE `evaluation` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `dX` double(8,2) NOT NULL,
  `dS` double(8,2) NOT NULL,
  `dFC` double(8,2) NOT NULL,
  `dImg` double(8,2) NOT NULL,
  `dCap` double(8,2) NOT NULL,
  `dUVC` double(8,2) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `evaluation`
--

INSERT INTO `evaluation` (`id`, `dX`, `dS`, `dFC`, `dImg`, `dCap`, `dUVC`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 3, '2022-01-26 20:03:06', '2022-01-26 20:03:06'),
(2, 1.00, 0.00, 0.50, 1.00, 1.00, 1.00, 4, '2022-01-26 20:03:06', '2022-01-26 20:03:06'),
(3, 1.00, 1.00, 1.00, 1.00, 1.00, 1.00, 3, '2022-01-26 20:12:54', '2022-01-26 20:12:54'),
(4, 1.00, 0.00, 0.50, 1.00, 1.00, 1.00, 4, '2022-01-26 20:12:54', '2022-01-26 20:12:54'),
(5, 0.00, 0.97, 1.00, 0.90, 0.90, 0.86, 8, '2022-02-01 14:09:12', '2022-02-01 14:09:12'),
(6, 0.00, 0.95, 0.99, 1.00, 0.99, 0.99, 9, '2022-02-01 14:09:12', '2022-02-01 14:09:12'),
(7, 0.00, 1.00, 0.99, 0.85, 0.90, 0.86, 10, '2022-02-01 14:09:12', '2022-02-01 14:09:12'),
(8, 0.00, 0.97, 1.00, 0.90, 0.90, 0.86, 8, '2022-02-01 14:10:12', '2022-02-01 14:10:12'),
(9, 0.00, 0.95, 0.99, 1.00, 0.99, 0.99, 9, '2022-02-01 14:10:12', '2022-02-01 14:10:12'),
(10, 0.00, 1.00, 0.99, 0.85, 0.90, 0.86, 10, '2022-02-01 14:10:12', '2022-02-01 14:10:12');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `first_paramters`
--

CREATE TABLE `first_paramters` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `Cap0` int(11) NOT NULL,
  `UVC0` int(11) NOT NULL,
  `Img0` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `first_paramters`
--

INSERT INTO `first_paramters` (`id`, `Cap0`, `UVC0`, `Img0`, `product_id`, `created_at`, `updated_at`) VALUES
(1, 2000, 2, 1, 2, '2021-11-23 07:46:25', '2021-11-23 07:46:25');

-- --------------------------------------------------------

--
-- Table structure for table `game`
--

CREATE TABLE `game` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `game`
--

INSERT INTO `game` (`id`, `name`, `created_at`, `updated_at`) VALUES
(2, 'game1', '2021-11-14 12:22:22', '2021-11-14 12:22:22'),
(3, 'game2', '2021-11-14 16:57:09', '2021-11-14 16:57:09'),
(4, 'game3', '2021-11-14 16:58:39', '2021-11-14 16:58:39'),
(5, 'game4', '2021-11-14 17:00:18', '2021-11-14 17:00:18'),
(6, 'game5', '2022-01-12 21:06:32', '2022-01-12 21:06:32'),
(7, 'لعبة الاء', '2022-01-29 10:47:37', '2022-01-29 10:47:37');

-- --------------------------------------------------------

--
-- Table structure for table `main_parameters`
--

CREATE TABLE `main_parameters` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `N` int(11) NOT NULL,
  `M` int(11) NOT NULL,
  `T` int(11) NOT NULL,
  `R` double(8,2) NOT NULL,
  `tau` double(8,2) NOT NULL,
  `alpha` int(11) NOT NULL,
  `gamma` double(8,2) NOT NULL,
  `beta` double(8,2) NOT NULL,
  `omega` double(8,2) NOT NULL,
  `lambda` double(8,2) NOT NULL DEFAULT 0.50,
  `mu` double(8,2) NOT NULL DEFAULT 1.50,
  `theta` double(8,2) NOT NULL DEFAULT 2.50,
  `delta` int(11) NOT NULL DEFAULT 1,
  `eta` double(8,2) NOT NULL DEFAULT 0.50,
  `game_id` int(11) NOT NULL,
  `phase_num` int(11) NOT NULL,
  `X0` int(11) NOT NULL,
  `FC0` int(11) NOT NULL,
  `S0` int(11) NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `main_parameters`
--

INSERT INTO `main_parameters` (`id`, `N`, `M`, `T`, `R`, `tau`, `alpha`, `gamma`, `beta`, `omega`, `lambda`, `mu`, `theta`, `delta`, `eta`, `game_id`, `phase_num`, `X0`, `FC0`, `S0`, `updated_at`, `created_at`) VALUES
(1, 2, 2, 1, 0.20, 0.50, 5, 0.30, 0.60, 0.50, 0.50, 1.50, 2.50, 1, 0.50, 2, 3, 100000, 0, 0, '2021-11-16 11:44:57', '2021-11-16 11:44:57'),
(2, 2, 2, 2, 0.20, 0.50, 5, 0.30, 0.60, 0.50, 0.50, 1.50, 2.50, 1, 0.50, 4, 3, 100000, 0, 0, '2021-11-23 15:15:37', '2021-11-23 15:15:37'),
(3, 2, 2, 0, 0.20, 0.50, 5, 0.30, 0.60, 0.50, 0.50, 1.50, 2.50, 1, 0.50, 3, 3, 2000000, 2, 1000, '2021-11-25 11:13:48', '2021-11-25 11:13:48'),
(4, 10, 2, 0, 0.20, 0.50, 5, 0.30, 0.60, 0.50, 0.50, 1.50, 2.50, 1, 0.50, 6, 3, 2000000, 2, 1000, '2022-01-12 21:06:51', '2022-01-12 21:06:51'),
(5, 3, 2, 0, 0.20, 0.50, 5, 0.30, 0.60, 0.50, 0.50, 1.50, 2.50, 1, 0.50, 7, 2, 2000000, 400000, 1000, '2022-01-29 10:50:49', '2022-01-29 10:50:49');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2021_11_07_201921_laratrust_setup_tables', 1),
(5, '2021_11_14_140700_create_game_table', 1),
(6, '2021_11_15_135259_create_company_game_table', 2),
(7, '2021_11_16_125454_create_main_parameters_table', 3),
(8, '2021_11_17_193046_create_first_paramters_table', 4),
(9, '2021_11_17_211430_create_product_characters_table', 5),
(10, '2021_11_22_141528_create_decisions_table', 6),
(12, '2021_11_24_214747_update_main_parameters1_table', 7),
(13, '2021_11_24_215217_update_product_chacter1_table', 7),
(14, '2022_01_10_172135_create_net_profits_table', 8),
(15, '2022_01_14_160522_create_divs_table', 9),
(16, '2022_01_17_214521_update_properties_table', 10),
(17, '2022_01_17_214707_update_parameters_table', 10),
(18, '2022_01_26_203936_create_evaluation_table', 11),
(19, '2022_01_27_200404_create_pointers_table', 12);

-- --------------------------------------------------------

--
-- Table structure for table `net_profits`
--

CREATE TABLE `net_profits` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `Npr` double(8,2) NOT NULL,
  `Dpr` double(8,2) NOT NULL DEFAULT 0.00,
  `Div_` float DEFAULT NULL,
  `phase` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `net_profits`
--

INSERT INTO `net_profits` (`id`, `Npr`, `Dpr`, `Div_`, `phase`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 12.00, 0.00, 0.4, 1, 5, '2022-01-10 16:56:42', '2022-01-14 22:37:38'),
(2, 0.00, 0.00, 0.5, 1, 6, '2022-01-10 16:56:42', '2022-01-14 22:37:10'),
(5, 0.00, 0.00, NULL, 1, 5, '2022-01-14 22:41:50', '2022-01-14 22:41:50'),
(6, 0.00, 0.00, NULL, 1, 6, '2022-01-14 22:41:50', '2022-01-14 22:41:50'),
(7, 0.00, 0.00, NULL, 1, 5, '2022-01-16 22:45:36', '2022-01-16 22:45:36'),
(8, 0.00, 0.00, NULL, 1, 6, '2022-01-16 22:45:36', '2022-01-16 22:45:36'),
(13, 0.00, 0.00, 0.4, 1, 11, '2022-01-17 01:49:53', '2022-01-17 01:49:53'),
(14, 0.00, 0.00, 0.5, 1, 22, '2022-01-17 01:49:53', '2022-01-17 01:49:53'),
(17, 0.44, 0.18, 0.4, 1, 3, '2022-01-17 20:17:07', '2022-01-27 19:32:44'),
(18, 0.00, 0.00, 0.5, 1, 4, '2022-01-17 20:17:07', '2022-01-27 10:25:49'),
(19, 0.00, 0.00, 0.3, 2, 3, '2022-01-21 22:00:55', '2022-01-21 22:01:57'),
(20, 0.00, 0.00, 0.6, 2, 4, '2022-01-21 22:00:55', '2022-01-21 22:02:35'),
(21, 0.00, 0.00, 0.2, 3, 3, '2022-01-21 22:15:19', '2022-01-21 22:16:39'),
(22, 0.00, 0.00, 0.1, 3, 4, '2022-01-21 22:15:19', '2022-01-21 22:17:48'),
(39, -150000.00, 0.00, 0.4, 1, 8, '2022-01-31 19:28:26', '2022-01-31 19:28:26'),
(40, -263600.00, 0.00, 0.4, 1, 9, '2022-01-31 19:28:26', '2022-01-31 19:28:26'),
(41, 160000.00, 64000.00, 0.4, 1, 10, '2022-01-31 19:28:26', '2022-01-31 19:29:26'),
(42, 96900.00, 19380.00, 0.1, 2, 8, '2022-02-01 13:50:53', '2022-02-01 14:20:54'),
(43, 67900.00, 6790.00, 0.1, 2, 9, '2022-02-01 13:50:53', '2022-02-01 13:53:42'),
(44, -105000.00, 0.00, 0.1, 2, 10, '2022-02-01 13:50:53', '2022-02-01 13:51:27');

-- --------------------------------------------------------

--
-- Table structure for table `parameters`
--

CREATE TABLE `parameters` (
  `id` int(11) NOT NULL,
  `Cap` int(11) DEFAULT NULL,
  `UVC` int(11) DEFAULT NULL,
  `Img` float NOT NULL,
  `phase` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `parameters`
--

INSERT INTO `parameters` (`id`, `Cap`, `UVC`, `Img`, `phase`, `product_id`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 2000, 2, 0.5004, 1, 1, 3, '2022-01-17 20:17:07', '2022-01-19 21:24:20'),
(2, 1000, 1, 0.5007, 1, 2, 3, '2022-01-17 20:17:07', '2022-01-19 21:24:20'),
(3, 2000, 2, 0.5005, 1, 1, 4, '2022-01-17 20:17:07', '2022-01-19 21:24:20'),
(4, 1000, 1, 0.5007, 1, 2, 4, '2022-01-17 20:17:07', '2022-01-19 21:24:20'),
(5, 2006, 2, 0.2542, 2, 1, 3, '2022-01-21 22:00:55', '2022-01-21 22:07:58'),
(6, 2006, 2, 0.25435, 2, 2, 3, '2022-01-21 22:00:55', '2022-01-21 22:07:58'),
(7, 2006, 2, 0.25425, 2, 1, 4, '2022-01-21 22:00:55', '2022-01-21 22:07:58'),
(8, 2006, 2, 0.25445, 2, 2, 4, '2022-01-21 22:00:55', '2022-01-21 22:07:58'),
(9, 2007, 2, 0.1281, 3, 1, 3, '2022-01-21 22:15:19', '2022-01-21 22:16:46'),
(10, 2006, 2, 0.128175, 3, 2, 3, '2022-01-21 22:15:19', '2022-01-21 22:16:46'),
(11, 2012, 2, 0.131125, 3, 1, 4, '2022-01-21 22:15:19', '2022-01-21 22:17:53'),
(12, 2012, 2, 0.131225, 3, 2, 4, '2022-01-21 22:15:19', '2022-01-21 22:17:53'),
(13, NULL, NULL, 0.5004, 12, 1, 3, '2022-01-22 11:48:32', '2022-01-22 11:48:32'),
(14, NULL, NULL, 0.5007, 16, 2, 3, '2022-01-22 11:48:32', '2022-01-22 11:48:32'),
(15, NULL, NULL, 0.5005, 13, 1, 4, '2022-01-22 11:48:32', '2022-01-22 11:48:32'),
(16, NULL, NULL, 0.5007, 123, 2, 4, '2022-01-22 11:48:32', '2022-01-22 11:48:32'),
(45, 2123, 375, 0.75, 1, 9, 8, '2022-01-31 19:28:26', '2022-01-31 19:31:36'),
(46, 2098, 380, 0.8, 1, 10, 8, '2022-01-31 19:28:26', '2022-01-31 19:31:36'),
(47, 2061, 388, 1, 1, 9, 9, '2022-01-31 19:28:26', '2022-01-31 19:43:21'),
(48, 2623, 275, 0.8, 1, 10, 9, '2022-01-31 19:28:26', '2022-01-31 19:43:21'),
(49, 2086, 383, 0.75, 1, 9, 10, '2022-01-31 19:28:26', '2022-01-31 19:43:21'),
(50, 2123, 375, 0.75, 1, 10, 10, '2022-01-31 19:28:26', '2022-01-31 19:43:21'),
(51, 2184, 364, 0.625, 2, 9, 8, '2022-02-01 13:50:53', '2022-02-01 13:53:50'),
(52, 2159, 369, 0.7, 2, 10, 8, '2022-02-01 13:50:53', '2022-02-01 13:53:50'),
(53, 2153, 371, 0.775, 2, 9, 9, '2022-02-01 13:50:53', '2022-02-01 13:53:50'),
(54, 2727, 264, 0.7, 2, 10, 9, '2022-02-01 13:50:53', '2022-02-01 13:53:50'),
(55, 2159, 370, 0.625, 2, 9, 10, '2022-02-01 13:50:53', '2022-02-01 13:53:50'),
(56, 2227, 357, 0.625, 2, 10, 10, '2022-02-01 13:50:53', '2022-02-01 13:53:50');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `role_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permission_user`
--

CREATE TABLE `permission_user` (
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `user_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `phase`
--

CREATE TABLE `phase` (
  `phase_number` int(11) DEFAULT NULL,
  `created_at` date DEFAULT NULL,
  `phase_name` varchar(100) DEFAULT NULL,
  `id` int(11) NOT NULL,
  `game_id` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `phase`
--

INSERT INTO `phase` (`phase_number`, `created_at`, `phase_name`, `id`, `game_id`) VALUES
(1, NULL, 'phase1', 1, 2),
(2, NULL, 'phase2', 2, 2),
(3, NULL, 'phase3', 3, 2);

-- --------------------------------------------------------

--
-- Table structure for table `pointers`
--

CREATE TABLE `pointers` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `TSpl` double(8,2) NOT NULL,
  `TMkg` double(8,2) NOT NULL,
  `TInv` double(8,2) NOT NULL,
  `TD` double(8,2) NOT NULL,
  `Avp` double(8,2) NOT NULL,
  `phase` int(11) NOT NULL,
  `product_id` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pointers`
--

INSERT INTO `pointers` (`id`, `TSpl`, `TMkg`, `TInv`, `TD`, `Avp`, `phase`, `product_id`, `created_at`, `updated_at`) VALUES
(1, 100.00, 9.00, 10.00, 24.00, 55.00, 1, 1, '2022-01-27 18:15:10', '2022-01-27 18:15:10'),
(2, 121.00, 14.00, 8.00, 49.00, 40.50, 1, 2, '2022-01-27 18:15:10', '2022-01-27 18:15:10');

-- --------------------------------------------------------

--
-- Table structure for table `product_characters`
--

CREATE TABLE `product_characters` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Gr` double(8,2) NOT NULL,
  `epsilon` double(8,2) NOT NULL,
  `pbv` double(8,2) NOT NULL,
  `rho` double(8,2) NOT NULL,
  `fl` double(8,2) NOT NULL,
  `sz` double(8,2) NOT NULL,
  `game_id` int(11) NOT NULL,
  `Cap0` int(11) NOT NULL,
  `UVC0` int(11) NOT NULL,
  `Img0` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `product_characters`
--

INSERT INTO `product_characters` (`id`, `name`, `Gr`, `epsilon`, `pbv`, `rho`, `fl`, `sz`, `game_id`, `Cap0`, `UVC0`, `Img0`, `created_at`, `updated_at`) VALUES
(1, 'pro2', 0.05, 1.20, 0.25, 0.25, 0.03, 0.75, 2, 2000, 2, 1, '2021-11-18 12:17:16', '2021-11-18 12:17:16'),
(2, 'pro3', 0.05, 1.20, 0.25, 0.25, 0.02, 0.75, 2, 2000, 2, 1, '2021-11-19 17:28:26', '2021-11-19 17:28:26'),
(5, 'sddssd', 0.05, 1.20, 0.25, 0.25, 0.02, 0.75, 4, 2000, 2, 1, '2021-11-25 11:47:26', '2021-11-25 11:47:26'),
(6, 'dsdsds', 0.05, 1.20, 0.25, 0.25, 0.02, 0.75, 4, 2000, 2, 1, '2021-11-25 11:47:26', '2021-11-25 11:47:26'),
(7, 'pros1', 0.05, 1.20, 0.25, 0.25, 0.02, 0.75, 6, 2000, 2, 1, '2022-01-12 21:07:39', '2022-01-12 21:07:39'),
(8, 'pros2', 0.05, 1.20, 0.25, 0.25, 0.02, 0.75, 6, 2000, 2, 1, '2022-01-12 21:07:39', '2022-01-12 21:07:39'),
(9, 'منتج1', 0.05, 1.20, 0.25, 0.25, 0.02, 0.75, 7, 2000, 400, 1, '2022-01-29 10:53:53', '2022-01-29 10:53:53'),
(10, 'منتج2', 0.05, 1.20, 0.25, 0.25, 0.02, 0.75, 7, 2000, 400, 1, '2022-01-29 10:53:53', '2022-01-29 10:53:53');

-- --------------------------------------------------------

--
-- Table structure for table `properties`
--

CREATE TABLE `properties` (
  `id` int(11) NOT NULL,
  `phase` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `X` int(11) NOT NULL,
  `FC` int(11) NOT NULL,
  `S` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `properties`
--

INSERT INTO `properties` (`id`, `phase`, `user_id`, `X`, `FC`, `S`, `created_at`, `updated_at`) VALUES
(1, 1, 4, 100000, 6, 0, '2022-01-17 19:47:50', '2022-01-17 19:47:50'),
(2, 1, 3, 100001, 3, 10, '2022-01-19 19:40:26', '2022-01-19 19:40:26'),
(5, 2, 3, 5, 30, 5, '2022-01-21 22:07:58', '2022-01-21 22:07:58'),
(6, 2, 4, 0, 38, 0, '2022-01-21 22:07:58', '2022-01-21 22:07:58'),
(7, 3, 3, 2000, 6, 5, '2022-01-21 22:16:46', '2022-01-21 22:16:46'),
(8, 3, 4, 0, 10, 10, '2022-01-21 22:17:53', '2022-01-21 22:17:53'),
(11, 1, 8, 1850000, 422100, 978, '2022-01-31 19:31:36', '2022-01-31 19:31:36'),
(12, 1, 9, 1736400, 468400, 960, '2022-01-31 19:43:21', '2022-01-31 19:43:21'),
(13, 1, 10, 2096000, 420900, 1043, '2022-01-31 19:43:21', '2022-01-31 19:43:21'),
(14, 2, 8, -19380, 411562, 998, '2022-02-01 13:53:50', '2022-02-01 13:53:50'),
(15, 2, 9, -6790, 416858, 972, '2022-02-01 13:53:50', '2022-02-01 13:53:50'),
(16, 2, 10, 0, 416796, 1027, '2022-02-01 13:53:50', '2022-02-01 13:53:50');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `role_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `user_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `x` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`, `x`) VALUES
(1, 'Alaa', 'alaa@gmail.com', NULL, '', NULL, NULL, NULL, '2022-01-06 09:47:20'),
(2, 'شركة 1', 'admin1@admin.com', NULL, '$2y$10$g6qPXFwbwO7s/wfKqYAPJOlBwB.GOJBAWe98kLI3PcP7AjeT/.PQe', NULL, '2021-11-15 11:49:40', '2021-11-15 11:49:40', '2022-01-06 09:47:20'),
(3, 'comp2', 'comp2@admin.com', NULL, '$2y$10$nbuZHYYAS9hZGOeAuswUB.pxSdRg3EW54MrFsT5p7W/hYNp8ah1xy', NULL, '2021-11-15 12:02:02', '2021-11-15 12:02:02', '2022-01-06 09:47:20'),
(4, 'comp1', 'comp1@admin.com', NULL, '$2y$10$nbuZHYYAS9hZGOeAuswUB.pxSdRg3EW54MrFsT5p7W/hYNp8ah1xy', NULL, '2021-11-20 11:18:33', '2021-11-20 11:18:33', '2022-01-06 09:47:20'),
(5, '', '', NULL, '', NULL, NULL, NULL, '2022-01-06 09:47:37'),
(6, 'fad1', 'fad1@admin.com', NULL, '$2y$10$MrY8.sW5GMjD9EGoY2txqO2KmztCgld8TC0Xk2sSZlPsNNHlT/ni.', NULL, '2022-01-12 21:11:12', '2022-01-12 21:11:12', '2022-01-12 23:11:12'),
(7, 'fad2', 'fad2@admin.com', NULL, '$2y$10$qGRwM7hKmBd1z7svuCFO5e30ZxD8jxJZynz5d5UHDJ4ctSwzMiwSq', NULL, '2022-01-12 21:11:46', '2022-01-12 21:11:46', '2022-01-12 23:11:46'),
(8, '1alaa', 'alaa@admin.com', NULL, '$2y$10$hQo3xHslLpqX8xYMFIFxWOy8iESlvve7xGShiQ2vLG7xLhGSgWxpK', NULL, '2022-01-29 11:18:52', '2022-01-29 11:18:52', '2022-01-29 13:18:52'),
(9, 'alaa2', 'alaa2@admin.com', NULL, '$2y$10$ZpamHP0n.qYzrg8Fv7ieMOMhvwHC.e4SOfgEyNQZhHFJt/dyLEFD.', NULL, '2022-01-29 11:19:58', '2022-01-29 11:19:58', '2022-01-29 13:19:58'),
(10, 'alaa3', 'alaa3@admin.com', NULL, '$2y$10$aBUQt3QGsiit8bMb0XNGCOgP/WFQlpGfZrJk8pPbrLEVMHco2ZIlu', NULL, '2022-01-29 11:20:29', '2022-01-29 11:20:29', '2022-01-29 13:20:29');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `company_game`
--
ALTER TABLE `company_game`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `decisions`
--
ALTER TABLE `decisions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `evaluation`
--
ALTER TABLE `evaluation`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `first_paramters`
--
ALTER TABLE `first_paramters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `game`
--
ALTER TABLE `game`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `main_parameters`
--
ALTER TABLE `main_parameters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `net_profits`
--
ALTER TABLE `net_profits`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `parameters`
--
ALTER TABLE `parameters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `permission_user`
--
ALTER TABLE `permission_user`
  ADD PRIMARY KEY (`user_id`,`permission_id`,`user_type`),
  ADD KEY `permission_user_permission_id_foreign` (`permission_id`);

--
-- Indexes for table `phase`
--
ALTER TABLE `phase`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_phase_1` (`game_id`);

--
-- Indexes for table `pointers`
--
ALTER TABLE `pointers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_characters`
--
ALTER TABLE `product_characters`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `properties`
--
ALTER TABLE `properties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`user_id`,`role_id`,`user_type`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `company_game`
--
ALTER TABLE `company_game`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `decisions`
--
ALTER TABLE `decisions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `evaluation`
--
ALTER TABLE `evaluation`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `first_paramters`
--
ALTER TABLE `first_paramters`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `game`
--
ALTER TABLE `game`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `main_parameters`
--
ALTER TABLE `main_parameters`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `net_profits`
--
ALTER TABLE `net_profits`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;

--
-- AUTO_INCREMENT for table `parameters`
--
ALTER TABLE `parameters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `phase`
--
ALTER TABLE `phase`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `pointers`
--
ALTER TABLE `pointers`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `product_characters`
--
ALTER TABLE `product_characters`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `properties`
--
ALTER TABLE `properties`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `permission_user`
--
ALTER TABLE `permission_user`
  ADD CONSTRAINT `permission_user_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
