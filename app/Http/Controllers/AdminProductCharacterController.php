<?php

namespace App\Http\Controllers;

use App\Models\Game;
use App\Models\MainParamter;
use App\Models\ProductCharacter;
use Illuminate\Http\Request;

class AdminProductCharacterController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products=ProductCharacter::all();
        return view('admin.products_characters.index' , ['products'=>$products]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($game_id)
    {
        $main_parameters = MainParamter::where('game_id',$game_id)->first();
        if($main_parameters)
        {
            return view('admin.products_characters.create',compact('game_id','main_parameters'));
        }
        else
        {
            return redirect('admin/game')->withErrors(['error-message' => '  يجب إضافة المحددات العامة للعبة أولاً !!']);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $product = new ProductCharacter();
        for($i=0;$i<count($request->name);$i++)
        {
            $product = new ProductCharacter();
            $product->name = $request->name[$i];
            $product->Gr = $request->gr[$i];
            $product->epsilon = $request->epsilon[$i];
            $product->pbv = $request->pbv[$i];
            $product->rho = $request->rho[$i];
            $product->fl = $request->fl[$i];
            $product->sz = $request->sz[$i];
            $product->game_id = $request->game_id;
            $product->Cap0 = $request->Cap0;
            $product->UVC0 = $request->UVC0;
            $product->Img0 = $request->Img0;
            $product->save();
        }
        $request->session()->flash('alert-success', 'تم إضافة المنتجات  بنجاح!!');
        return redirect('/admin/products_character');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ProductCharacter  $productCharacter
     * @return \Illuminate\Http\Response
     */
    public function show($product_id)
    {

        $productCharacters = ProductCharacter::where('id',$product_id)->first();
        $game = Game::find($productCharacters->game_id);

        return view('admin.products_characters.show', compact('productCharacters' ,'game_id','game'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ProductCharacter  $productCharacter
     * @return \Illuminate\Http\Response
     */
    public function edit( $id)

    {
        $product=ProductCharacter::find($id);
        return view('admin.products_characters.edit' , ['product'=>$product]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ProductCharacter  $productCharacter
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {


        {
            $product =  ProductCharacter::find($id);
            $product->name = $request->name;
            $product->gr = $request->gr;
            $product->epsilon = $request->epsilon;
            $product->pbv = $request->pbv;
            $product->rho = $request->rho;
            $product->fl = $request->fl;
            $product->sz = $request->sz;
            $product->save();
        }
        $request->session()->flash('alert-success', 'تم تعديل المنتجات  بنجاح!!');
        return redirect('admin/products_character/index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ProductCharacter  $productCharacter
     * @return \Illuminate\Http\Response
     */
    public function destroy( $id)
    {
$product =ProductCharacter::find($id);
$product->delete();
return redirect('admin/products_character/index')->with(session()->flash('alert-success', 'تم حذف  المنتج  بنجاح!!'));
    }
}
