<?php

namespace App\Http\Controllers;

use App\Models\MainParamter;
use Illuminate\Http\Request;
use App\Models\Game;
class AdminMainParametersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $main_parameters = MainParamter::all();
        return view('admin.main_parameters.index',compact('main_parameters'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($game_id)
    {
        $main_parameters = MainParamter::find($game_id);
        if($main_parameters)
        {
            return redirect('admin/game')->withErrors(['error-message' => 'لقد أضفت محددات عامة مسبقاً لهذه اللعبة']);
        }
        else
        {
            $game = Game::find($game_id);
            return view('admin.main_parameters.create',compact('game_id','game'));
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //var_dump($request->all());
        $main_parameters = new MainParamter();
        $main_parameters->N = $request->N;
        $main_parameters->M = $request->M;
        $main_parameters->phase_num = $request->phase_num;
        $main_parameters->T = $request->T;
        $main_parameters->R = $request->R;
        $main_parameters->tau = $request->tau;
        $main_parameters->alpha = $request->alpha;
        $main_parameters->gamma = $request->gamma;
        $main_parameters->beta = $request->beta;
        $main_parameters->omega = $request->omega;
        $main_parameters->game_id = $request->game_id;
        $main_parameters->X0 = $request->X0;
        $main_parameters->FC0 = $request->FC0;
        $main_parameters->S0 = $request->S0;

        if ($main_parameters->save()){

            $request->session()->flash('alert-success', 'تم إضافة محددات عامة بنجاح!!');
            return redirect('/admin/main_parameters');
        }else{
            $request->session()->flash('alert-danger', 'لم يتم إضافة محددات عامة !! حاول لاحقاً');
            return redirect('/admin/main_parameters');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show($game_id)
    {
        $game = Game::find($game_id);
        //$main_parameter = MainParamter::find($game_id);

        $main_parameter = MainParamter::where('game_id',$game_id)->first();


        return view('admin.main_parameters.show', compact('main_parameter' ,'game_id', 'game'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
