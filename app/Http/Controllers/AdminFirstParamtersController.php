<?php

namespace App\Http\Controllers;


use App\Models\FirstParamters;
use App\Models\MainParamter;
use App\Models\ProductCharacter;
use Illuminate\Http\Request;

class AdminFirstParamtersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $main_parameters = MainParamter::all();
        return view('admin.first_parameters.index',compact('main_parameters'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($product_id)
    {
        $product = ProductCharacter::find($product_id);
        return view('admin.first_parameters.create',compact('product_id','product'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //var_dump($request->all());
        $first_parameters = new FirstParamters();
        $first_parameters->X0 = $request->X0;
        $first_parameters->FC0 = $request->FC0;
        $first_parameters->S0 = $request->S0;
        $first_parameters->Cap0 = $request->Cap0;
        $first_parameters->UVC0 = $request->UVC0;
        $first_parameters->Img0 = $request->Img0;
        $first_parameters->product_id = $request->product_id;
        $first_parameters->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FirstParamters  $firstParamters
     * @return \Illuminate\Http\Response
     */
    public function show(FirstParamters $firstParamters)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FirstParamters  $firstParamters
     * @return \Illuminate\Http\Response
     */
    public function edit(FirstParamters $firstParamters)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FirstParamters  $firstParamters
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FirstParamters $firstParamters)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FirstParamters  $firstParamters
     * @return \Illuminate\Http\Response
     */
    public function destroy(FirstParamters $firstParamters)
    {
        //
    }
}
