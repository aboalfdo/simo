<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FunctionsController extends Controller
{
    public function __construct()
    {
        //$this->middleware('auth');
    }
    //----------Calculate DOP(j)
    public function Exp($M, $Q, $UVC_, $Mkg, $Inv, $FC_)
    {
        $Exp = 0;
        for($i=0; $i<$M; $i++)
        {
            $Exp += (($Q[$i]*$UVC_[$i]) + $Mkg[$i] +$Inv[$i]);
        }
        $Exp += $FC_;
        return $Exp;
    }
    //----------Calculate DOP(i,j)
    public function DOP( $P, $UVC0)
    {
        $DOP = 0;$mu = 1.5; $theta = 2.5;
        $DOP = min($P , $UVC0*$mu*$theta);
        return $DOP;
    }
    //----------Calculate AP(i,j)
    public function AP($M, $Q, $DOP)
    {
        $AP = 0;
        $sum_Q_DOP = 0;
        $sum_Q = 0;
        for($i=0; $i<$M; $i++)
        {
            $sum_Q_DOP += $Q[$i]*$DOP[$i];
            $sum_Q += $Q[$i];
        }
        $AP = $sum_Q_DOP  / $sum_Q;
        return $AP;
    }

    //----------Calculate D(i,j)
    public function D($N ,$Cap0 ,$Sz ,$AP , $UVC0 ,$epsilon)
    {
        $D = 0;
        $D = $N * $Cap0 * $Sz/pow(($AP/($UVC0*1.5)), $epsilon);
        return $D;
    }
    //----------Calculate g(i,j)
    public function g($M, $Mkg, $N, $X0)
    {
        $g = 0;
        $sumOfMkg = 0;
        for($i=0;$i<$M;$i++)
        {
            $sumOfMkg += $Mkg[$i];
        }
        $g = $sumOfMkg/($N*$X0);
        return $g;
    }


    //----------Calculate left domain(i,j)
    public function ED1($D, $Gr, $g, $j, $fl)
    {
        $ED1 = 0;
        return $ED1 = $D*pow((1+$Gr),($j-1))*(1+$g)*(1-$fl);
    }
    //----------Calculate right domain(i,j)
    public function ED2($D, $Gr, $g, $j, $fl)
    {
        $ED2 = 0;
        return $ED2 = $D*pow((1+$Gr),($j-1))*(1+$g)*(1+$fl);
    }

    public function ED($ED1, $delta_D)
    {
        $xi = rand(0, 10) / 10;
        //var_dump($xi);
        $ED = $ED1 + $xi*$delta_D;
        //var_dump($ED);
        return $ED;
    }

    public function Mkg_star($Mkg, $N, $X0, $M)
    {
        $sumOfMkg = 0;
        for($i=0;$i<$M;$i++)
        {
            $sumOfMkg += $Mkg[$i];
        }
        return max($sumOfMkg/$N , $X0/(10*$M));
    }

    public function Img($Mkg, $Mkg_star, $omega, $Img_)
    {
        $Img = $omega * ($Mkg/$Mkg_star)+(1-$omega)*$Img_;
        return $Img;
    }

    public function PF($AP, $P, $epsilon)
    {
        $PF = pow($AP/$P, (1+$epsilon));
        return $PF;
    }
    public function CC($rho, $PF, $Img)
    {
        $CC = (1-$rho)*$PF +$rho*$Img;
        return $CC;
    }

    public function MS($CC, $CC_i, $DOP, $P, $epsilon, $M)
    {
        $MS = 0; $sumOfCC = 0;
        for($i=0;$i<$M;$i++)
        {
            $sumOfCC += $CC[$i];
        }
        if($sumOfCC != 0 && $P != 0)
        {
            $MS = ($CC_i/$sumOfCC)*pow($DOP/$P , (1+$epsilon));
        }
        else{
            $MS = ($CC_i/0.1)*pow($DOP/0.1 , (1+$epsilon));
        }
        return $MS;
    }

    public function DS($Q, $MS, $ED)
    {
        $CD = 0;
        $CD = min($Q , $MS*$ED);
        return $CD;
    }

    public function CD($ED, $DS, $M)
    {
        $CD = 0; $sumOfDS = 0;
        for($i=0;$i<$M;$i++)
        {
            $sumOfDS += $DS[$i];
        }
        $CD = $ED - $sumOfDS;
        return $CD;
    }

    public function FS($Q, $DS ,$MS, $CD)
    {
        $FS = 0;
        $FS = min($Q , $DS+$MS*$CD);
        $FS = round($FS);
        return $FS;
    }

    public function Rev($FS, $P, $Q, $UVC0, $PBV, $M)
    {
        $Rev = 0;
        for($i=0;$i<$M;$i++)
        {
            $Rev += $FS[$i]*$P[$i] +($Q[$i]-$FS[$i])*$UVC0[$i]*$PBV[$i];
        }
        return $Rev;
    }

    public function DA($j, $alpha, $M, $Inv)
    {
        $x=0;
        if($j==1)
        {
            return $x;
        }
        elseif (1<$j && $j<= $alpha)
        {
            for($i=0;$i<$M;$i++)
            {
                for($k=1;$k<=$j-1;$k++)
                {
                    $x += $Inv[$i]/$alpha;
                }
            }
            return $x;
        }
        else{
            for($i=0;$i<$M;$i++)
            {
                for($k=$j-$alpha;$k<=$j-1;$k++)
                {
                    $x += $Inv[$i]/$alpha;
                }
            }
            return $x;
        }
    }

    public function TC($Exp, $DA , $Inv ,$M)
    {
        $TC = 0;
        $TC = $Exp + $DA; $X = 0;
        for($i=0;$i<$M;$i++)
        {
            $X += $Inv[$i];
        }
        $TC -= $X;
        return $TC;
    }
    public function EBIT($Rev, $TC)
    {
        return $Rev - $TC;
    }

    public function Dec($Exp, $X_, $tau, $Rev )
    {
        $Dec = 0;
        $Dec = max($Exp-$X_-($tau*$Rev), 0)/(1+3*$tau);
        return $Dec;
    }
    public function Int($Dec, $R)
    {
        return $Dec * $R;
    }

    public function Tax($EBIT, $Int, $T )
    {
        $Tax = 0;
        $Tax = max($EBIT-$Int, 0)*$T;
        return $Tax;
    }
    public function NPr($EBIT, $Int, $Tax)
    {
        return $EBIT-$Int-$Tax;
    }
    public function DPr($NPr, $Div)
    {
        return max($NPr,0)*$Div;
    }
    //---------------------------------------------
    public function X($X_, $Rev, $Exp, $Int, $Tax, $Drp, $Fn=0)
    {
        return $X_+$Rev-$Exp-$Int-$Tax-$Drp-$Fn;
    }
    public function S($S_, $gamma, $Npr, $beta, $Dpr, $X0)
    {
        return $S_*(1+($gamma*$Npr+$beta*$Dpr)/$X0);
    }
    public function Cap($Cap_, $Inv, $UVC0)
    {
        return round($Cap_+max( ($Inv/(2*$UVC0))-2, 0));
    }
    public function UVC($UVC_, $Cap, $Cap_, $UVC0)
    {
        return max( $UVC_*(2-$Cap/$Cap_), $UVC0/2);
    }
    public function FC($M, $FC_, $Cap, $Cap_)
    {
        $sum = 0;
        $FC = $FC_;
        for($i=0;$i<$M;$i++)
        {
            $sum += $Cap[$i]/$Cap_[$i];
        }
        $FC = $FC*$sum/$M;
        return $FC;
    }

    //-------------------------------------
    public function TSpl($M, $Q)
    {
        $TSpl = 0;
        $sum_Q = 0;
        for($i=0; $i<$M; $i++)
        {
            $sum_Q += $Q[$i];
        }
        $TSpl = $sum_Q;
        return $TSpl;
    }
    public function TMkg($M, $Mkg)
    {
        $TMkg = 0;
        $sum_Mkg = 0;
        for($i=0; $i<$M; $i++)
        {
            $sum_Mkg += $Mkg[$i];
        }
        $TMkg = $sum_Mkg;
        return $TMkg;
    }
    public function TInv($M, $Inv)
    {
        $TInv = 0;
        $sum_Inv = 0;
        for($i=0; $i<$M; $i++)
        {
            $sum_Inv += $Inv[$i];
        }
        $TInv = $sum_Inv;
        return $TInv;
    }
    public function TD($M, $FS)
    {
        $TD = 0;
        $sum_FS = 0;
        for($i=0; $i<$M; $i++)
        {
            $sum_FS += $FS[$i];
        }
        $TD = $sum_FS;
        return $TD;
    }
    public function AvP($M, $Q, $P)
    {
        $AvP = 0;
        $sum_Q_P = 0;
        $sum_Q = 0;
        for($i=0; $i<$M; $i++)
        {
            $sum_Q_P += $Q[$i]*$P[$i];
            $sum_Q += $Q[$i];
        }
        $AvP = $sum_Q_P  / $sum_Q;
        return $AvP;
    }


    //evaluation
    public function dX($X, $X_arr)
    {
        if(max($X_arr)==0)
        {
            return  0;
        }
        else{
            return $X/max($X_arr);
        }
    }

    public function dS1($S, $S_arr)
    {
        return $S/max($S_arr);
    }
    public function dFC($FC, $FC_arr)
    {
        return min($FC_arr)/$FC;
    }
    public function dImg_i($Img, $Img_arr)
    {
        return $Img/max($Img_arr);
    }

    public function dImg( $Img_i_arr,$M)
    {
        $dImg_i = 0;
        for($i=0; $i<$M; $i++)
        {
            $dImg_i +=$Img_i_arr[$i];
        }
        $dImg_i *= 1/$M;
        return $dImg_i;
    }
    public function dCap_i($Cap, $Cap_arr)
    {
        return $Cap/max($Cap_arr);
    }
    public function dCap( $dCap_i_arr,$M)
    {
        $dCap_i = 0;
        for($i=0; $i<$M; $i++)
        {
            $dCap_i +=$dCap_i_arr[$i];
        }
        $dCap_i *= 1/$M;
        return $dCap_i;
    }
    public function dUVC_i($UVC, $UVC_arr)
    {
        return min($UVC_arr)/$UVC;
    }
    public function dUVC( $dUVC_i_arr,$M)
    {
        $dUVC_i = 0;
        for($i=0; $i<$M; $i++)
        {
            $dUVC_i +=$dUVC_i_arr[$i];
        }
        $dUVC_i *= 1/$M;
        return $dUVC_i;
    }
    public function perf( $dX,$dS)
    {
        $perf = 0;
        $perf = $dX + $dS;
        $perf *=1/2;
        return $perf;
    }
    public function Comp( $dX,$dFC, $dImg, $dCap, $dUVC)
    {
        $comp = ($dX + $dFC + 2*($dImg+$dCap+$dUVC));
        $comp *=1/8;
        return $comp;
    }
    public function Scor( $lambda, $perf, $comp)
    {
        $scor = ($lambda* $perf + (1-$lambda)*$comp);
        $scor *=100;
        return $scor;
    }


    public function test()
    {
//        $Q= [1,1];
//        $UVC= [1,1];
//        $Mkg= [1,1];
//        $Inv= [1,1];
//        $FC= [1,1];
//        $r=$this->Exp(2,$Q,$UVC,$Mkg,$Inv,$FC);
//        echo $r;

//        $r1 = $this->FS(3.3,4,2.3,0.2);
//        echo $r1;

         // $Q = [1,1];
          //$DOP = [1,2];
          $r2 = $this->dX(10,[2,2,10]);
         echo $r2;

        //$this->ED(1,2);
//        $Q= [4000 , 1000];
//    $M=2;
//    $P=[2000,7000];
//    $r1 = $this->AvP($M, $Q, $P);
//    echo $r1;
    }

}
