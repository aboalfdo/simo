<?php

namespace App\Http\Controllers;

use App\Model\Pointers;
use Illuminate\Http\Request;

class PointersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Pointers  $pointers
     * @return \Illuminate\Http\Response
     */
    public function show(Pointers $pointers)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Pointers  $pointers
     * @return \Illuminate\Http\Response
     */
    public function edit(Pointers $pointers)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Pointers  $pointers
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Pointers $pointers)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Pointers  $pointers
     * @return \Illuminate\Http\Response
     */
    public function destroy(Pointers $pointers)
    {
        //
    }
}
