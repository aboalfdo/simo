<?php

namespace App\Http\Controllers;
use App\Models\CompanyGame;
use App\Models\MainParamter;
use App\Models\ProductCharacter;
use App\Models\Game;
use App\Models\Decision;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\User;
use Illuminate\Support\Facades\DB;
class AdminDefaultDecisionController extends Controller
{
    //
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $game_id = 0;
    protected $game;
    protected $user;
    public function __construct()
    {
        $this->middleware('auth');

        $this->middleware(function ($request, $next) {
            //get user
            $this->user= Auth::user();
            //get game
            $game  =  CompanyGame::where('user_id', $this->user->id)->first();
            $this->game_id = $game->game_id;
            $this->game = $game;
            return $next($request);
        });
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        //$user_id = $this->user->id;
        $company_name = $this->user->name;
         //get products ids
        $products = ProductCharacter::select('id')->where('game_id', $this->game_id)->get();
        $product_ids = array();
        foreach($products as $product)
        {
            array_push($product_ids,$product->id) ;
        }
        $decisions = Decision::where('user_id', $user_id)->whereIn('product_id', $product_ids)->get();
        return view('decisions.index', ['decisions' =>$decisions,'company_name'=>$company_name]);
    }

    public function home()
    {
        return view('admin.index');
    }

    public function create()
    {

        $game_id = $this->game_id;
        $user_id = $this->user->id;
        $companies = CompanyGame::where('game_id', $game_id)->get();
        $companiesNotMadeDecision = array();
        foreach($companies as $company)
        {
            $x = true;
            foreach ($decisions as $decision)
            {
                if($company->user_id==$decision->user_id)
                {
                    $x = true;
                    break;
                }
                else
                {
                    $x = false;
                }
            }
            if(!$x)
            {
                array_push($companiesNotMadeDecision, $company->user_id);
            }
        }
        $companiesNot = User::whereIn('id', $companiesNotMadeDecision)->get();

        $maxvp = Decision::where('user_id',$user_id )->max('p');
        $company_name = $this->user->name;
        //get M
        $main_parameters = MainParamter::where('game_id',$game_id)->first();
        //get bigger phase
        $phase =  Decision::where('user_id',$user_id )->max('phase');
        //phase is number
        if(!is_numeric($phase))
        {
            //game not begins
            $phase = 0;
        }
        //get products
        $products = ProductCharacter::where('game_id', $this->game_id)->get();

        return view('admin.decisions.create',compact('main_parameters','game_id','phase','company_name','products','maxvp'));
    }
    public function store(Request $request, $game_id, $phase)
    {
        $user_id = $this->user->id;
 //get game
 $game = Game::find($game_id);
 //get products ids
 $products = ProductCharacter::select('id')->where('game_id', $game_id)->get();
 $product_ids = array();

        for($i=0;$i<count($request->product_id);$i++)
        {
            $decisions = new Decision();
            $decisions->q = $request->q[$i];
            $decisions->p = $request->p[$i];
            $decisions->mkg = $request->mkg[$i];
            $decisions->inv = $request->inv[$i];
            $decisions->div = $request->div[$i];
            $decisions->product_id = $request->product_id[$i];
            $decisions->phase =  $request->phase;
            $decisions->user_id =  $user_id;
            $decisions->save();
        }
        $request->session()->flash('alert-success', 'تم إضافة القرارات  بنجاح!!');


    return redirect("admin/game/phase/gameId}/{phase}");
}
    public function GetGame()
    {
        $games = Game::all();
        return view ('companies.game.index');

    }
    public function div(Request $request)
    {
        $decisions = new Decision();
        $decisions->div = $request->div;
        $decisions->save();

    }
}
