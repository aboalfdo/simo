<?php

namespace App\Http\Controllers;

use App\Http\Controllers\FunctionsController;
use App\Models\CompanyGame;
use App\Models\Evaluation;
use App\Models\Game;
use App\Models\MainParamter;
use App\Models\Parameters;
use App\Models\ProductCharacter;
use App\Models\Property;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class EvaluationController extends Controller
{
    public function index($game_id)
    {
        $game = Game::find($game_id);
        $main_parameters = MainParamter::where('game_id',$game_id)->first();
        $this->evaluations($game_id, $main_parameters->phase_num);
        $this->evaluations_parameters($game_id, $main_parameters->phase_num);

        $companies = CompanyGame::select('user_id')->where('game_id', $game_id)->get();

        $fc = new FunctionsController();
        $perf = array(); $comp = array();$Scor = array();
        for($c=0 ; $c<$main_parameters->N ; $c++)
        {
            $evaluations = Evaluation::where('user_id',$companies[$c]->user_id)->first();
            $perf[$companies[$c]->user_id] = $fc->perf($evaluations->dX, $evaluations->dS);

            $comp[$companies[$c]->user_id] = $fc->Comp($evaluations->dX, $evaluations->dFC, $evaluations->dImg, $evaluations->dCap, $evaluations->dUVC);

            $Scor[$companies[$c]->user_id] = $fc->Scor($main_parameters->lambda, $perf[$companies[$c]->user_id], $comp[$companies[$c]->user_id] );
        }
        $data = ['perf'=>$perf , 'Comp'=>$comp , 'Scor'=>$Scor ,'game'=>$game, 'companies'=>$companies];
        return view('admin.evaluation.index', $data);
    }
    public function evaluations($game_id, $phase)
    {
        //get game
        $game = Game::find($game_id);
        $products = ProductCharacter::where('game_id', $game_id)->get();
        $product_ids = array();
        foreach($products as $product)
        {
            array_push($product_ids,$product->id) ;
        }

        $companies = CompanyGame::select('user_id')->where('game_id', $game_id)->get();

        $fc = new FunctionsController();

        $main_parameters = MainParamter::where('game_id',$game_id)->first();
        $dX_arr = array();$dS_arr = array(); $dFC_arr = array();

        $X = array(); $S = array(); $FC = array();
        for($c=0 ; $c<$main_parameters->N ; $c++)
        {
            $property = Property::where('phase', $phase)->where('user_id',$companies[$c]->user_id)->first();
            array_push($X, $property->X);
            array_push($S, $property->S);
            array_push($FC, $property->FC);
        }
        for($c=0 ; $c<$main_parameters->N ; $c++)
        {
            $property = Property::where('phase', $phase)->where('user_id',$companies[$c]->user_id)->first();
            $dX = $fc->dX($property->X, $X);
            $dS = $fc->dS1($property->S, $S);
            $dFC = $fc->dFC($property->FC, $FC);

            $dX_arr[$companies[$c]->user_id] = $dX;
            $dS_arr[$companies[$c]->user_id] = $dS;
            $dFC_arr[$companies[$c]->user_id] = $dFC;
            //var_dump($dFC_arr);// $dFC_arr;
            //echo "<br>";
            $this->saveToSession('dX',$dX, $companies[$c]->user_id);
            $this->saveToSession('dS',$dS, $companies[$c]->user_id);
            $this->saveToSession('dFC',$dFC, $companies[$c]->user_id);
        }
        //$this->evaluations_parameters($game_id, $phase);
        //return view('admin.evaluation.index',compact('game_id','game'));
    }

    public function evaluations_parameters($game_id, $phase)
    {
        $fc = new FunctionsController();
        $product_ids = array();
        $products = ProductCharacter::where('game_id', $game_id)->get();
        foreach($products as $product)
        {
            array_push($product_ids,$product->id) ;
        }
        $main_parameters = MainParamter::where('game_id',$game_id)->first();
        $companies = CompanyGame::select('user_id')->where('game_id', $game_id)->get();
        $dImg_i = array(); $dCap_i = array(); $dUVC_i = array();
        //for each product
        for($i=0; $i<$main_parameters->M ; $i++)
        {
            $Img = array(); $Cap = array(); $UVC = array();
            //for each company
            for($c=0 ; $c<$main_parameters->N ; $c++)
            {
                $parameters = Parameters::where('phase',$phase)->where('user_id',$companies[$c]->user_id)->where('product_id', $product_ids[$i])->first();
                array_push($Img, $parameters->Img );
                array_push($Cap, $parameters->Cap );
                array_push($UVC, $parameters->UVC);
            }
            for($c=0 ; $c<$main_parameters->N ; $c++)
            {
                $parameters = Parameters::where('phase',$phase)->where('user_id',$companies[$c]->user_id)->where('product_id', $product_ids[$i])->first();
                $dImg_item = $fc->dImg_i($parameters->Img, $Img);
                $dCap_item = $fc->dCap_i($parameters->Cap, $Cap);
                $dUVC_item = $fc->dUVC_i($parameters->UVC, $UVC);
                $dImg_i[$i][$c] = $dImg_item;
                $dCap_i[$i][$c] = $dCap_item;
                $dUVC_i[$i][$c] = $dUVC_item;
            }
        }
        $dImgCompanies = array(); $dCapCompanies = array(); $dUVCCompanies = array();
        for($c=0 ; $c<$main_parameters->N ; $c++)
        {
            $dImgCompany = array(); $dCapCompany = array(); $dUVCCompany = array();
            for($i=0; $i<$main_parameters->M ; $i++)
            {
                array_push($dImgCompany,$dImg_i[$i][$c] );
                array_push($dCapCompany,$dCap_i[$i][$c] );
                array_push($dUVCCompany, $dUVC_i[$i][$c]);
            }
            $dImg = $fc->dImg($dImgCompany, $main_parameters->M );
            $dCap = $fc->dCap($dCapCompany, $main_parameters->M);
            $dUVC = $fc->dUVC($dUVCCompany, $main_parameters->M);

            $dImgCompanies[$companies[$c]->user_id] = $dImg;
            $dCapCompanies[$companies[$c]->user_id] = $dCap;
            $dUVCCompanies[$companies[$c]->user_id] = $dUVC;
            $dX = session('dX');
            $dS = session('dS');
            $dFC = session('dFC');
            //echo $companies[$c]->user_id."=".$dX[$companies[$c]->user_id];
            $evaluation = new Evaluation();
            $evaluation->dX = $dX[$companies[$c]->user_id];
            $evaluation->dS = $dS[$companies[$c]->user_id];
            $evaluation->dFC = $dFC[$companies[$c]->user_id];
            $evaluation->dImg = $dImg;
            $evaluation->dCap = $dCap;
            $evaluation->dUVC = $dUVC;
            $evaluation->user_id = $companies[$c]->user_id;
            $evaluation->save();

            //echo $companies[$c]->user_id."=".$dS[$companies[$c]->user_id];
        }
        session::forget('dX');
        session::forget('dS');
        session::forget('dFC');
        //var_dump(session('dX'));
    }
    public function saveToSession($key, $value, $user_id)
    {
        if( !session($key))
        {
            $x[$user_id] = $value;
            session([$key => $x]);
        }
        else{
            $arr = session($key);
            $arr[$user_id] = $value;
            session([$key => $arr]);
        }
    }

}
