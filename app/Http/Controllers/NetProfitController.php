<?php

namespace App\Http\Controllers;

use App\Net_profit;
use Illuminate\Http\Request;

class NetProfitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Net_profit  $net_profit
     * @return \Illuminate\Http\Response
     */
    public function show(Net_profit $net_profit)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Net_profit  $net_profit
     * @return \Illuminate\Http\Response
     */
    public function edit(Net_profit $net_profit)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Net_profit  $net_profit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Net_profit $net_profit)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Net_profit  $net_profit
     * @return \Illuminate\Http\Response
     */
    public function destroy(Net_profit $net_profit)
    {
        //
    }
}
