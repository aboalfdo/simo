<?php

namespace App\Http\Controllers;
use App\Models\CompanyGame;
use App\Models\MainParamter;
use App\Models\Net_profit;
use App\Models\ProductCharacter;
use App\Models\User;
use Illuminate\Http\Request;
use App\Models\Game;
use App\Models\Decision;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Session;
//use Auth;
class DecisionsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $game_id = 0;
    protected $game;
    protected $user;
    public function __construct()
    {
        $this->middleware('auth');

        $this->middleware(function ($request, $next) {
            //get user
            $this->user= Auth::user();
            //get game
            $game  =  CompanyGame::where('user_id', $this->user->id)->first();
            $this->game_id = $game->game_id;
            $this->game = $game;
            return $next($request);
        });
    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user_id = $this->user->id;
        $company_name = $this->user->name;
         //get products ids
        $products = ProductCharacter::select('id')->where('game_id', $this->game_id)->get();
        $product_ids = array();
        foreach($products as $product)
        {
            array_push($product_ids,$product->id) ;
        }
        $decisions = Decision::where('user_id', $user_id)->whereIn('product_id', $product_ids)->get();
        return view('decisions.index', ['decisions' =>$decisions,'company_name'=>$company_name]);
    }

    public function home()
    {
        return view('admin.index');
    }

    public function create()
    {
        $game_id = $this->game_id;
        $user_id = $this->user->id;
        $company_name = $this->user->name;
        //get M
        $main_parameters = MainParamter::where('game_id',$game_id)->first();
        //get bigger phase
        $phase =  Decision::where('user_id',$user_id )->max('phase');
        //phase is number
        if(!is_numeric($phase))
        {
            //game not begins
            $phase = 0;
        }
        //get products
        $products = ProductCharacter::where('game_id', $this->game_id)->get();

        return view('decisions.create',compact('main_parameters','game_id','phase','company_name','products'));
    }
    public function store(Request $request)
    {
        $user_id = $this->user->id;
        for($i=0;$i<count($request->product_id);$i++)
        {
            $decisions = new Decision();
            $decisions->q = $request->q[$i];
            $decisions->p = $request->p[$i];
            $decisions->mkg = $request->mkg[$i];
            $decisions->inv = $request->inv[$i];
            //$decisions->div = $request->div[$i];
            $decisions->product_id = $request->product_id[$i];
            $decisions->phase =  $request->phase;
            $decisions->user_id =  $user_id;
            $decisions->save();
        }
        $request->session()->flash('alert-success', 'تم إضافة القرارات  بنجاح!!');
        return redirect('/decisions');
    }
    public function start()
    {
        $game_id = $this->game_id;
        //get game
        $game = Game::find($game_id);
        $user_id = $this->user->id;
        $company_name = $this->user->name;
        //get M
        $main_parameters = MainParamter::where('game_id',$game_id)->first();
        return view ('companies.game.start',compact('main_parameters','game'));

    }

    public function insert_div($phase)
    {
        $game_id = $this->game_id;
        $user_id = $this->user->id;
        $company_name = $this->user->name;
        //get game
        $game = Game::find($game_id);
        //get products ids
        $products = ProductCharacter::select('id')->where('game_id', $game_id)->get();
        $product_ids = array();
        foreach($products as $product)
        {
            array_push($product_ids,$product->id) ;
        }

        $Npr = Net_profit::where('user_id',$user_id)->where('phase',$phase)->first();

        return view('companies.game.phase', [ 'game'=>$game, 'phase'=>$phase,'Npr'=>$Npr,'company_name'=>$company_name]);

    }
    public function div(Request $request)
    {
        $user_id = $this->user->id;
        //get record by phase and company id
        $item = Net_profit::where('user_id',$user_id)->where('phase',$request->phase)->first();
        $div = Net_profit::find($item->id);
        $div->Div_ = $request->div;
        if ($div->save()){
            $request->session()->flash('alert-success', 'تم إضافة نسبة توزيع الأرباح!!');
            return redirect('/dprComp/'.$request->phase);
        }else{
            $request->session()->flash('alert-danger', 'لم تضاف نسبة توزيع الأرباح !! حاول لاحقاً');
            return redirect('/dprComp/'.$request->phase);
        }
    }
    public function pointers($phase)
    {
        var_dump($phase);
//        $user_id = $this->user->id;
//        //get record by phase and company id
//        $item = Net_profit::where('user_id',$user_id)->where('phase',$request->phase)->first();
//        $div = Net_profit::find($item->id);
//        $div->Div_ = $request->div;
//        if ($div->save()){
//            $request->session()->flash('alert-success', 'تم إضافة نسبة توزيع الأرباح!!');
//            return redirect('/div/'.$request->phase);
//        }else{
//            $request->session()->flash('alert-danger', 'لم تضاف نسبة توزيع الأرباح !! حاول لاحقاً');
//            return redirect('/div/'.$request->phase);
//        }
       // return view('companies.game.pointers', [ 'dpr'=>$dpr,'game'=>$game, 'phase'=>$phase,'phase'=>$phase,'company_name'=>$company_name]);
    }
    public function dpr($phase)
    {

        $game_id = $this->game_id;
        $user_id = $this->user->id;
        $company_name = $this->user->name;
        //get game
        $game = Game::find($game_id);
        //get game
        $game = Game::find($game_id);
        $dpr = Net_profit::where('user_id',$user_id)->where('phase',$phase)->first();
        return view('companies.game.dpr', [ 'dpr'=>$dpr,'game'=>$game, 'phase'=>$phase,'phase'=>$phase,'company_name'=>$company_name]);
    }
}
