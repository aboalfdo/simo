<?php

namespace App\Http\Controllers;

use App\Http\Controllers\FunctionsController;

use App\Models\Pointers;
use App\Models\CompanyGame;
use App\Models\Decision;
use App\Models\Game;
use App\Models\MainParamter;
use App\Models\Net_profit;
use App\Models\Parameters;
use App\Models\ProductCharacter;
use App\Models\User;
use App\Models\Property;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
//use Illuminate\Support\Facades\Session;
use App\Models\Quantity;
use Session;
class AdminPointersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public $Rev_arr;
    public function __construct()
    {

    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */


    public function index($game_id, $phase)
    {
        //get game
        $game = Game::find($game_id);
        $main_parameters = MainParamter::where('game_id',$game_id)->first();
        //var_dump( $main_parameters->N);
        $companies = CompanyGame::select('user_id')->where('game_id', $game_id)->get();

        $products = ProductCharacter::where('game_id', $game_id)->get();
        $product_ids = array(); $companies_ids = array();
        foreach($products as $product) {
            array_push($product_ids,$product->id) ;
        }
        for($c=0 ; $c<$main_parameters->N ; $c++)
        {
            //echo "c: ".$c;echo $companies[$c]->user_id;echo "<br>";
            array_push($companies_ids,$companies[$c]->user_id) ;
            $this->continue_calculations($game_id, $phase, $companies[$c]->user_id);
            ///array_push($properties, $pro);
            //pointers
            $this->market_pointers($game_id, $phase);
        }
        //session::forget('Rev');session::forget('Exp');session::forget('Int');session::forget('Tax');
        //get X S FC
        $properties = Property::whereIn('user_id', $companies_ids)->where('phase', $phase)->get();

        $params = Parameters::whereIn('user_id',$companies_ids )->where('phase', $phase)->get();
        $numOfPhases = $main_parameters->phase_num;
        $quantities =   Quantity::whereIn('user_id',$companies_ids)->where('phase', $phase)->get();
        $pointers = Pointers::where('phase', $phase)->whereIn('product_id', $product_ids)->get();
        //var_dump($quantities);
        $decisions = Decision::where('phase', $phase)->whereIn('product_id', $product_ids)->get();

        $data = [
            'game'  => $game,
            'game_id'  => $game_id,
            'phase' => $phase,
            'numOfPhases' => $numOfPhases,
            'properties' => $properties,
            'parameters' => $params, 'pointers'=>$pointers ,
            'quantities'=>$quantities ,'companies'=>$companies,
            'decisions'=>$decisions ];
        return view('admin.game.pointers', $data);
    }
    public function home()
    {
        if(is_null(session('admin_id'))){
            return redirect('admin-login')->send();
        }
        return view('admin.index');
    }

    public function continue_calculations($game_id, $phase, $user_id)
    {
        $main_parameters = MainParamter::where('game_id',$game_id)->first();
        $net_profit = Net_profit::where('user_id',$user_id)->where('phase',$phase)->first();
        //get products ids
        $products = ProductCharacter::where('game_id', $game_id)->get();
        $decisions = Decision::where('phase', $phase)->where('user_id', $user_id)->get();
        $product_ids = array();
        foreach($products as $product) {
            array_push($product_ids,$product->id) ;
        }
        $Caps = array(); $Caps_ = array();
        $fc = new FunctionsController();
        //for each product
        for($i=0; $i<$main_parameters->M ; $i++)
        {
            $item = Parameters::where('user_id',$user_id)->where('product_id', $product_ids[$i])->where('phase', $phase)->first();
            //echo "i=:".$i;echo "<br>";
            if($item->Img && !$item->UVC)
            {
                if($phase == 1)
                {
                   // echo "calculate uvc,cap";echo "<br>";
                    $Cap = $fc->Cap($products[$i]->Cap0, $decisions[$i]->Inv, $products[$i]->UVC0);
                    array_push($Caps, $Cap);
                    //here Cap is Cap0
                    array_push($Caps_, $products[$i]->Cap0);
                    $UVC = $fc->UVC($products[$i]->UVC0, $Cap, $products[$i]->Cap0,$products[$i]->UVC0 );
                    //update
                    $item->UVC  = $UVC;
                    $item->Cap = $Cap;
                    $item->save();
                }
                else{
                    $parameter_ = Parameters::where('product_id', $product_ids[$i])->where('user_id', $user_id)->where('phase', $phase-1)->first();
                    $Cap = $fc->Cap($parameter_->Cap, $decisions[$i]->Inv, $products[$i]->UVC0);
                    array_push($Caps, $Cap);
                    array_push($Caps_, $parameter_->Cap);
                    $UVC = $fc->UVC($parameter_->UVC, $Cap, $parameter_->Cap,$products[$i]->UVC0 );
                    //update
                    $item->UVC  = $UVC;
                    $item->Cap = $Cap;
                    $item->save();
                }
            }
        }
        $X = Property::where('phase',$phase)->where('user_id',$user_id)->first();
        if(!$X){
            $Rev = session('Rev');
            $Exp = session('Exp');
            $Int = session('Int');
            $Tax = session('Tax');
            //echo "Rev: ";
            //var_dump($Rev);echo "<br>";
            //session::forget('Rev');session::forget('Exp');session::forget('Int');session::forget('Tax');
            $property = new Property();
            if($phase==1)
            {
                // echo "cal X S FC";echo "<br>";
                $X = $fc->X($main_parameters->X0, $Rev[$user_id][$phase], $Exp[$user_id][$phase], $Int[$user_id][$phase], $Tax[$user_id][$phase], $net_profit->Dpr);
                $S = $fc->S($main_parameters->S0, $main_parameters->gamma, $net_profit->Npr, $main_parameters->beta,  $net_profit->Dpr, $main_parameters->X0);
                $FC = $fc->FC($main_parameters->M, $main_parameters->FC0, $Caps, $Caps_ );
                $property->X = $X;
                $property->S = $S;
                $property->FC = $FC;
                $property->phase = 1;
                $property->user_id = $user_id;
                $property->save();
            }
            else{
                $property_ = Property::where('user_id', $user_id)->where('phase', $phase-1)->first();
                $X = $fc->X($property_->X_, $Rev[$user_id][$phase], $Exp[$user_id][$phase], $Int[$user_id][$phase], $Tax[$user_id][$phase], $net_profit->Dpr);
                $S = $fc->S($property_->S, $main_parameters->gamma, $net_profit->Npr, $main_parameters->beta,  $net_profit->Dpr, $main_parameters->X0);
                $FC = $fc->FC($main_parameters->M, $main_parameters->FC0, $Caps, $Caps_ );
                $property->X = $X;
                $property->S = $S;
                $property->FC = $FC;
                $property->phase = $phase;
                $property->user_id = $user_id;
                $property->save();
            }
        }
    }

    public function market_pointers($game_id ,$phase)
    {
        $main_parameters = MainParamter::where('game_id',$game_id)->first();
        $companies = CompanyGame::select('user_id')->where('game_id', $game_id)->get();
        $products = ProductCharacter::where('game_id', $game_id)->get();
        $fc = new FunctionsController();
        $product_ids = array();
        foreach($products as $product)
        {
            array_push($product_ids,$product->id) ;
        }
        //var_dump($product_ids);
        $pointers = Pointers::where('phase', $phase)->whereIn('product_id', $product_ids)->first();
        //var_dump($pointers);
        if(!$pointers)
        {
           // echo "in";
           // exit();
            //$Tspl = array(); $Tmkg = array(); $TInv = array(); $TD = array(); $Avp = array();
            $FS_full = session('FS');
            //var_dump($FS_full);echo "<br>";
            for($i=0; $i<$main_parameters->M ; $i++)
            {
                $Q = array(); $Mkg = array(); $Inv = array();$FS = array(); $P = array();
                for($c=0 ; $c<$main_parameters->N ; $c++) {
                    $decision = Decision::where('phase', $phase)->where('product_id',$product_ids[$i])->where('user_id', $companies[$c]->user_id)->first();
                    array_push($Q, $decision->Q);
                    array_push($Mkg, $decision->Mkg);
                    array_push($Inv, $decision->Inv);
                    array_push($P, $decision->P);
                    array_push($FS, $FS_full[$companies[$c]->user_id][$i]);
                }
                //var_dump($FS);echo "<br>";
                $Tspl_i = $fc->TSpl($main_parameters->M ,$Q);
                $TMkg_i = $fc->TMkg($main_parameters->M, $Mkg);
                $TInv_i = $fc->TInv($main_parameters->M, $Inv);
                $TD_i = $fc->TD($main_parameters->M, $FS);
                $Avp_i = $fc->AvP($main_parameters->M, $Q, $P);
                $x = new Pointers();
                $x->TSpl = $Tspl_i;
                $x->TMkg = $TMkg_i;
                $x->TInv = $TInv_i;
                $x->TD = $TD_i;
                $x->Avp = $Avp_i;
                $x->product_id = $product_ids[$i];
                $x->phase = $phase;
                $x->save();
            }
       }
        //echo "out";
        //session::forget('FS');
    }

    public function saveToSession($key, $value, $user_id, $phase)
    {
        if( !session($key))
        {
            $x[$user_id][$phase] = $value;
            session([$key => $x]);
        }else{
            $arr = session($key);
            $arr[$user_id][$phase] = $value;
            session([$key => $arr]);
        }
        \Illuminate\Support\Facades\Session::save();
    }
    public function saveToSessionWithoutPhase($key, $value, $user_id)
    {
        if( !session($key))
        {
            $x[$user_id] = $value;
            session([$key => $x]);
        }
        else{
            $arr = session($key);
            $arr[$user_id] = $value;
            session([$key => $arr]);
        }
        \Illuminate\Support\Facades\Session::save();
    }
}
