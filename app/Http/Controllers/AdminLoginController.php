<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use App\Models\CompanyGame;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Game;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class AdminLoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function login()
    {
        return view('admin.login'/*,compact('game_id','main_parameters')*/);

    }
    public function validation(Request $request)
    {
        //var_dump($request->all());
        $request->validate([
            'email' => 'required',
            'password' => 'required',
        ]);
        //$admin = Admin::where('email',$request->email)->where('password',Hash::make($request->password))->first();
        $admin = Admin::where('email',$request->email)->first();
        if (Hash::check($request->password, $admin->password))
        {
            $request->session()->put('admin_id',$admin->id );
            return redirect('admin-home');
        }
        else
        {
            return redirect('admin-login');
        }

    }

    public function logout()
    {
        session()->forget(['admin_id']);
        session()->flush();
        return redirect('admin-login');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //var_dump($request->all());
        $user = new User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->email = $request->email;
        if ($user->save()){
            $company_game = new CompanyGame();
            $company_game->user_id = $user->id;
            $company_game->game_id = $request->game_id;
            $company_game->save();
            $request->session()->flash('alert-success', 'تم إضافة شركة جديدة بنجاح!!');
            return redirect('/admin/companies');
        }else{
            $request->session()->flash('alert-danger', 'لم يتم إضافة أي شركة !! حاول لاحقاً');
            return redirect('/admin/companies');
        }
    }

}
