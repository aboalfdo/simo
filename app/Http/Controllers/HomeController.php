<?php

namespace App\Http\Controllers;

use App\Http\Controllers\FunctionsController;

use App\Models\Pointers;
use App\Models\CompanyGame;
use App\Models\Decision;
use App\Models\Game;
use App\Models\MainParamter;
use App\Models\Net_profit;
use App\Models\Parameters;
use App\Models\ProductCharacter;
use App\Models\User;
use App\Models\Property;
use App\Models\Quantity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;


//use Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public $Rev_arr;
    public function __construct()
    {

    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $games = Game::all();
        return view('admin.game.index',compact('games'));
    }

    public function home()
    {

        if(is_null(session('admin_id'))){
            return redirect('admin-login')->send();
        }
        return view('admin.index');
    }

    public function store(Request $request)
    {
//        $validator = Validator::make($request->all(), [
//            'name' => 'required',
//            'title' => 'required',
//        ]);
//        if ($validator->fails()) {
//            return redirect('jobs/create')
//                ->withErrors($validator)
//                ->withInput();
//        }

        $name = $request->input('game');

        $game = new Game();
        $game->name = $name;
        if ($game->save()){
            $request->session()->flash('alert-success', 'تم إضافة لعبة جديدة بنجاح!!');
            //\Illuminate\Support\Facades\Session::save();
            //var_dump(Session::flash('alert-success'));
            return redirect('/admin-home');
        }else{
            $request->session()->flash('alert-danger', 'لم تضاف أي لعبة !! حاول لاحقاً');
            //\Illuminate\Support\Facades\Session::save();
            return redirect('/admin-home');
        }
    }

    public function start($game_id)
    {
        $game = Game::find($game_id);
        //get M
        $main_parameters = MainParamter::where('game_id',$game_id)->first();
        $products = ProductCharacter::where('game_id', $game_id)->get();
        return view('admin.game.start',compact('main_parameters','game_id','game'));
    }
    public function phase($game_id, $phase)
    {
        //get game
        $game = Game::find($game_id);
        //get products ids
        $products = ProductCharacter::select('id')->where('game_id', $game_id)->get();
        $product_ids = array();
        foreach($products as $product)
        {
            array_push($product_ids,$product->id) ;
        }
        $decisions = Decision::where('phase', $phase)->whereIn('product_id', $product_ids)->get();
        //get $companies
        $companies = CompanyGame::where('game_id', $game_id)->get();
        $companiesNotMadeDecision = array();
        foreach($companies as $company)
        {
            $x = true;
            foreach ($decisions as $decision)
            {
                if($company->user_id==$decision->user_id)
                {
                    $x = true;
                    break;
                }
                else
                {
                    $x = false;
                }
            }
            if(!$x)
            {
                array_push($companiesNotMadeDecision, $company->user_id);
            }
        }
        $companiesNot = User::whereIn('id', $companiesNotMadeDecision)->get();
        return view('admin.game.phase', ['decisions' =>$decisions, 'game'=>$game, 'phase'=>$phase, 'companiesNot'=>$companiesNot]);
    }
    public function caculate_Npr($game_id, $phase)
    {
        $Npr = array();
        //get game
        $game = Game::find($game_id);
        //get products ids
        $products = ProductCharacter::where('game_id', $game_id)->get();
        $product_ids = array();
        foreach($products as $product)
        {
            array_push($product_ids,$product->id) ;
        }
        $companies = CompanyGame::select('user_id')->where('game_id', $game_id)->get();

        $fc = new FunctionsController();

        $main_parameters = MainParamter::where('game_id',$game_id)->first();
        $Mkg = array(); $Img = array();$AP = array(); $ED = array(); $MS = array(); $DS = array(); $CD = array();
        //for each company
        for($c=0 ; $c<$main_parameters->N ; $c++)
        {
            $item = Net_profit::where('user_id',$companies[$c]->user_id)->where('phase',$phase)->first();
            // if Npr calculated and saved already
            if(!$item)
            {
                //get $decisions by company
                $decisions = Decision::where('phase', $phase)->where('user_id', $companies[$c]->user_id)->get();
                $mkgs = array();$Qs = array();$DOPs = array() ;$CCs = array(); $DSs =array();$Ps = array();
                $UVC0s = array(); $PBVs = array(); $Invs = array(); $UVCs_ = array();
                for($i=0; $i<$main_parameters->M ; $i++)
                {
                    array_push($mkgs, $decisions[$i]->Mkg);
                    array_push($Qs, $decisions[$i]->Q);
                    array_push($Ps, $decisions[$i]->P);
                    array_push($UVC0s, $products[$i]->UVC0);
                    array_push($PBVs, $products[$i]->pbv);
                    array_push($Invs, $products[$i]->Inv);
                    $DOP = $fc->DOP($decisions[$i]->P , $products[$i]->UVC0 );
                    array_push($DOPs , $DOP);
                }
                //for each product
                for($i=0; $i<$main_parameters->M ; $i++)
                {
                    $Mkg[$c][$i] = $fc->Mkg_star($mkgs , $main_parameters->N , $main_parameters->X0 , $main_parameters->M);
                    $parameter = new Parameters();
                    if($phase == 1)
                    {
                        $Img[$c][$i] = $fc->Img($decisions[$i]->Mkg , $Mkg[$c][$i], $main_parameters->omega ,$products[$i]->Img0 );
                        //save Img in table Paramaters
                        $parameter->phase = 1;
                        $parameter->user_id = $companies[$c]->user_id;
                        $parameter->product_id = $product_ids[$i];
                        $parameter->Img = $Img[$c][$i];
                        $parameter->save();
                    }
                    else{
                        $parameter_ = Parameters::where('product_id', $product_ids[$i])->where('user_id', $companies[$c]->user_id)->where('phase', $phase-1)->first();
                        $Img[$c][$i] = $fc->Img($decisions[$i]->Mkg , $Mkg[$c][$i], $main_parameters->omega ,$parameter_->Img );
                        array_push($UVCs_, $parameter_->UVC);
                        //save Img in table Paramaters
                        $parameter->phase = $phase;
                        $parameter->user_id = $companies[$c]->user_id;
                        $parameter->product_id = $product_ids[$i];
                        $parameter->Img = $Img[$c][$i];
                        $parameter->save();
                    }
                    $AP[$c][$i] = $fc->AP($main_parameters->M , $Qs , $DOPs );
                    $PF[$c][$i] = $fc->PF($AP[$c][$i], $decisions[$i]->P, $products[$i]->epsilon );
                    $CC[$c][$i] = $fc->CC($products[$i]->rho, $PF[$c][$i], $Img[$c][$i] );
                    array_push($CCs, $CC[$c][$i]);
                }

                for($i=0; $i<$main_parameters->M ; $i++)
                {
                    $MS[$c][$i] = $fc->MS($CCs , $CC[$c][$i], $DOP, $decisions[$i]->P, $products[$i]->epsilon, $main_parameters->M );
                    $D[$c][$i] = $fc->D($main_parameters->N, $products[$i]->Cap0, $products[$i]->sz, $AP[$c][$i], $products[$i]->UVC0, $products[$i]->epsilon);
                    $g[$c][$i] = $fc->g($main_parameters->M, $mkgs, $main_parameters->N, $main_parameters->X0);
                    $ED1[$c][$i] = $fc->ED1($D[$c][$i], $products[$i]->Gr, $g[$c][$i], $phase,  $products[$i]->fl);
                    $ED2[$c][$i] = $fc->ED2($D[$c][$i], $products[$i]->Gr, $g[$c][$i], $phase, $products[$i]->fl);
                    $delta_D = $ED2[$c][$i] - $ED1[$c][$i];
                    $ED[$c][$i] = $fc->ED($ED1[$c][$i], $delta_D);
                    $DS[$c][$i] = $fc->DS( $decisions[$i]->Q, $MS[$c][$i], $ED[$c][$i]);
                    array_push($DSs,$DS[$c][$i]);
                }
                $FS = array();
                for($i=0; $i<$main_parameters->M ; $i++)
                {
                    $CD[$c][$i] = $fc->CD($ED[$c][$i], $DSs, $main_parameters->M);
                    $FS[$i] = $fc->FS($decisions[$i]->Q, $DS[$c][$i], $MS[$c][$i], $CD[$c][$i]);
                    $q = new Quantity();
                    $q->phase = $phase;
                    $q->user_id = $companies[$c]->user_id;
                    $q->product_id = $product_ids[$i];
                    $q->FS = $FS[$i];
                    $q->save();
                }
                $this->saveToSessionWithoutPhase('FS',$FS,$companies[$c]->user_id);
                $Exp = 0; $Dec = 0;
                $Rev = $fc->Rev($FS, $Ps, $Qs, $UVC0s, $PBVs, $main_parameters->M);
                $this->saveToSession('Rev',$Rev,$companies[$c]->user_id,$phase);
                $DA = $fc->DA($phase, $main_parameters->alpha, $main_parameters->M, $Invs);

                if($phase == 1){
                    $Exp = $fc->Exp($main_parameters->M, $Qs, $UVC0s, $mkgs, $Invs,$main_parameters->FC0 );
                    $this->saveToSession('Exp',$Exp,$companies[$c]->user_id,$phase);
                    $Dec = $fc->Dec($Exp, $main_parameters->X0, $main_parameters->tau, $Rev);
                }
                else{
                    //need testing in other phases
                    $property_ = Property::where('user_id', $companies[$c]->user_id)->where('phase', $phase-1)->first();
                    $Exp = $fc->Exp($main_parameters->M, $Qs, $UVCs_, $mkgs, $Invs, $property_->FC);
                    $this->saveToSession('Exp',$Exp,$companies[$c]->user_id,$phase);
                    $Dec = $fc->Dec($Exp, $property_->X, $main_parameters->tau, $Rev);
                }
                $TC = $fc->TC($Exp, $DA, $Invs, $main_parameters->M );
                $EBIT = $fc->EBIT($Rev,$TC);

                $Int = $fc->Int($Dec, $main_parameters->R);
                $this->saveToSession('Int', $Int, $companies[$c]->user_id,$phase);
                $Tax = $fc->Tax($EBIT, $Int, $main_parameters->T);
                $this->saveToSession('Tax', $Tax, $companies[$c]->user_id,$phase);

                $Npr_single = $fc->NPr($EBIT, $Int, $Tax);
                $npr = new Net_profit();
                //$item = Net_profit::where('user_id',$companies[$c]->user_id)->where('phase',$phase)->first();
                $npr->user_id = $companies[$c]->user_id;
                $npr->phase = $phase;
                $npr->Npr = $Npr_single;
                $npr->save();
            }
        }

        return redirect('admin/shownpr/'.$game_id.'/'.$phase);
        //return view('admin.game.npr', ['Npr' =>$Npr, 'game'=>$game, 'phase'=>$phase, 'company_details'=>$company_details]);
    }
    public function show_Npr($game_id, $phase)
    {
        //get game
        $game = Game::find($game_id);
        $company_details = array();
        $companies = CompanyGame::select('user_id')->where('game_id', $game_id)->get();
        $main_parameters = MainParamter::where('game_id',$game_id)->first();
        //get products ids
        $products = ProductCharacter::where('game_id', $game_id)->get();
        $product_ids = array(); $companies_ids = array();
        foreach($products as $product) {
            array_push($product_ids,$product->id) ;
        }
        $Npr = array(); $Dpr = array(); $properties = array(); $params = array();
        for($c=0 ; $c<$main_parameters->N ; $c++)
        {
            array_push($companies_ids,$companies[$c]->user_id) ;
            $company = User::where('id', $companies[$c]->user_id)->first();
            array_push($company_details, $company);
            //get Npr
            $item = Net_profit::where('user_id',$companies[$c]->user_id)->where('phase',$phase)->first();
            array_push($Npr, $item);
            $fc = new FunctionsController();
            if($item->Div_)
            {
                $Dpr_single = $fc->DPr($item->Npr,$item->Div_);
                array_push($Dpr, $Dpr_single);
                //save Dpr
                $item->Dpr = $Dpr_single;
                $item->save();
                //$this->continue_calculations($game_id, $phase, $companies[$c]->user_id);
            }
            else{
                // if there is company not entered Div
            }
        }
        $data = ['Npr' =>$Npr,
            'game'  => $game,
            'phase' => $phase,
            'company_details' => $company_details,
            'Dpr'   => $Dpr,
            /*'properties' => $properties,
            'parameters' => $params, 'pointers'=>$pointers*/];
        return view('admin.game.npr', $data);
    }


    public function saveToSession($key, $value, $user_id, $phase)
    {
        if( !session($key))
        {
            $x[$user_id][$phase] = $value;
            session([$key => $x]);

        }else{
            $arr = session($key);
            //$arr = Session::get($key);
            $arr[$user_id][$phase] = $value;
            session([$key => $arr]);
        }
        \Illuminate\Support\Facades\Session::save();
    }
    public function saveToSessionWithoutPhase($key, $value, $user_id)
    {
        if( !session($key))
        {
            $x[$user_id] = $value;
            session([$key => $x]);
        }
        else{
            $arr = session($key);
            $arr[$user_id] = $value;
            session([$key => $arr]);
        }
        \Illuminate\Support\Facades\Session::save();
    }

}
