<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    protected $table = 'game';
    protected $fillable = [
        'name'
    ];
    public function Company()
    {
        return $this->belongsToMany(CompanyGame::class);
    }
}
