<?php

namespace App\Models;
use App\Models\ProductCharacter;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Decision extends Model
{
    public function product()
    {
        return $this->belongsTo(ProductCharacter::class);
    }
    public function company()
    {
        return $this->belongsTo(User::class,'user_id');
    }

}
