<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Parameters extends Model
{
    //
    protected $table = 'parameters';

    public function product()
    {
        return $this->belongsTo(ProductCharacter::class, 'product_id');
    }
    public function company()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
