<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MainParamter extends Model
{
    protected $table = 'main_parameters';
    protected $fillable = [
        'N', 'M', 'T','R','tau','alpha','gamma',
        'beta','omega','lambda','mu','theta','delta','eta','game_id','phase_num','X0','FC0','S0'
    ];

    public function game(){
        return $this->hasOne('App\Models\Game', 'id', 'game_id');
    }
}
