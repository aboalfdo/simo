<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    protected $table = 'properties';
    public function company()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
