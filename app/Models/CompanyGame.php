<?php

namespace App\Models;
use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class CompanyGame extends Model
{
    protected $table = 'company_game';
    protected $fillable = [
        'user_id','game_id'
    ];
    public function company()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
