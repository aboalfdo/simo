<?php

namespace App\Models;
use App\Models\ProductCharacter;

use Illuminate\Database\Eloquent\Model;

class Pointers extends Model
{
    public function product()
    {
        return $this->belongsTo(ProductCharacter::class);
    }
}
